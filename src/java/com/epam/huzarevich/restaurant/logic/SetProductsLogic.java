/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.logic;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductCategoryDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductDAO;
import com.epam.huzarevich.restaurant.entities.Product;
import com.epam.huzarevich.restaurant.entities.ProductCategory;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**Sets products to their categories 
 *
 * @author al94
 */
public class SetProductsLogic {
    private static final DAOFactory DF=DAOFactory.getDAOFactory(DAOEnum.MYSQL);

    public static void setForClients(SessionWrapper sessionWrapper) throws PoolException, DAOException {
        IProductDAO dao= DF.getProductDao();
        List<Product> products = dao.getAll();
        IProductCategoryDAO pcDAO = DF.getProductCategoryDao();
        List<ProductCategory> categorys = pcDAO.getAll();
        Iterator <ProductCategory>it=categorys.iterator();
        while (it.hasNext()) {
            if(it.next().isVisible()==false){
                it.remove();
            }           
        }
        for (ProductCategory category : categorys) {
            List<Product> catProducts = new ArrayList<>();

            for (Product product : products) {

                if (category.getName().equals(product.getCategory().getName()) && product.isInMenu() == true) {
                    catProducts.add(product);

                }
            }
            category.setProducts(catProducts);
        }
        sessionWrapper.setAttribute("categoryProducts", categorys);
    }

    public static void setForAdmins(SessionWrapper sessionWrapper) throws PoolException, DAOException {
        IProductDAO dao= DF.getProductDao();
        List<Product> products = dao.getAll();
        IProductCategoryDAO pcDAO = DF.getProductCategoryDao();
        List<ProductCategory> categoryProducts = pcDAO.getAll();
        for (ProductCategory category : categoryProducts) {
            List<Product> catProducts = new ArrayList<>();
            for (Product product : products) {
                if (category.getName().equals(product.getCategory().getName())) {
                    catProducts.add(product);
                }
                category.setProducts(catProducts);
            }
            sessionWrapper.setAttribute("categoryProducts", categoryProducts);

        }
    }

}
