package com.epam.huzarevich.restaurant.logic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**Validation by regex
 *
 * @author al94
 */
public class ValidateParameterLogic {

    /**
     *Checks password
     * @param parameter
     * @return boolean 
     */
    public static boolean checkPassword(String parameter) {
        Pattern p = Pattern.compile("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}");
        Matcher m = p.matcher(parameter);
        return m.matches();
    }

    /**
     *checks name and surname
     * @param parameter
     * @return boolean
     */
    public static boolean checkNameSurname(String parameter) {
        Pattern p = Pattern.compile("[-A-ZА-Яa-zа-я]{1,30}");
        Matcher m = p.matcher(parameter);
        return m.matches();
    }

    /**checks login
     *
     * @param parameter
     * @return boolean
     */
    public static boolean checkLogin(String parameter) {
        Pattern p = Pattern.compile("^[a-zA-Z][a-zA-Z0-9_\\.]{2,20}$");
        Matcher m = p.matcher(parameter);
        return m.matches();
    }

    /**checks street
     *
     * @param parameter
     * @return boolean
     */
    public static boolean checkStreet(String parameter) {
        Pattern p = Pattern.compile("^[А-Яа-я _]*[А-Яа-я][А-Яа-я _]*$");
        Matcher m = p.matcher(parameter);
        return m.matches();
    }

    /**checks house
     *
     * @param parameter
     * @return boolean
     */
    public static boolean checkHouse(String parameter) {
        Pattern p = Pattern.compile("^[0-9][0-9/А-Яа-я]+");
        Matcher m = p.matcher(parameter);
        return m.matches();
    }

    /**checks apartment
     *
     * @param parameter
     * @return boolean
     */
    public static boolean checkApartment(String parameter) {
        Pattern p = Pattern.compile("^[0-9]+$");
        Matcher m = p.matcher(parameter);
        return m.matches();
    }

    /**checks phone
     *
     * @param parameter
     * @return
     */
    public static boolean checkPhone(String parameter) {
        Pattern p = Pattern.compile("[0-9]{9}");
        Matcher m = p.matcher(parameter);
        return m.matches();
    }

    /**checks product or categiry
     *
     * @param parameter
     * @return boolean
     */
    public static boolean checkProductOrCategoryName(String parameter) {
        Pattern p = Pattern.compile("[-&quot«A-ZА-Яa-zа-я\\s»]{1,50}");
        Matcher m = p.matcher(parameter);
        return m.matches();
    }
   

}
