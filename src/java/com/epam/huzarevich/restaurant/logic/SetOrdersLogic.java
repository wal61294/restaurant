/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.logic;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IOrderDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.entities.Order;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author al94
 */
public class SetOrdersLogic {

    private static final DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);

    /**Set orders for client
     *
     * @param wrapper
     * @param id
     * @throws PoolException
     * @throws DAOException
     */
    public static void setClientOrders(SessionWrapper wrapper, int id) throws PoolException, DAOException {
        IOrderDAO dao = df.getOrderDao();
        List<Order> orders = dao.getAllOrdersByUserId(id);
        List<Order> doneOrders = new ArrayList<>();
        List<Order> undoneOrders = new ArrayList<>();
        List<Order> cancelledOrders = new ArrayList<>();
        List<Order> disapprovedOrders = new ArrayList<>();
        List<Order> newOrders = new ArrayList<>();
        for (Order order : orders) {
            if (order.getOrderStatus().equals("done")) {
                doneOrders.add(order);

            } else if (order.getOrderStatus().equals("approved")) {
                undoneOrders.add(order);
            } else if (order.getOrderStatus().equals("new")) {
                newOrders.add(order);

            } else if (order.getOrderStatus().equals("cancelled")) {
                cancelledOrders.add(order);
            } else if (order.getOrderStatus().equals("disapproved")) {
                disapprovedOrders.add(order);
            }

        }
        wrapper.setAttribute("doneOrders", doneOrders);
        wrapper.setAttribute("undoneOrders", undoneOrders);
        wrapper.setAttribute("newOrders", newOrders);
        wrapper.setAttribute("cancelledOrders", cancelledOrders);
        wrapper.setAttribute("disapprovedOrders", disapprovedOrders);
    }

    /**Set orders for admin
     *
     * @param wrapper
     * @throws PoolException
     * @throws DAOException
     */
    public static void setOrdersForAdmin(SessionWrapper wrapper) throws PoolException, DAOException {
        List<Order> orders=setUsersForOrders();
        List<Order> doneOrders = new ArrayList<>();
        List<Order> undoneOrders = new ArrayList<>();
        List<Order> cancelledOrders = new ArrayList<>();
        List<Order> disapprovedOrders = new ArrayList<>();
        List<Order> newOrders = new ArrayList<>();
        for (Order order : orders) {
            if (order.getOrderStatus().equals("done")) {
                doneOrders.add(order);

            } else if (order.getOrderStatus().equals("approved")) {
                undoneOrders.add(order);
            } else if (order.getOrderStatus().equals("new")) {
                newOrders.add(order);

            } else if (order.getOrderStatus().equals("cancelled")) {
                cancelledOrders.add(order);
            } else if (order.getOrderStatus().equals("disapproved")) {
                disapprovedOrders.add(order);
            }

        }
        wrapper.setAttribute("doneOrders", doneOrders);
        wrapper.setAttribute("undoneOrders", undoneOrders);
        wrapper.setAttribute("newOrders", newOrders);
        wrapper.setAttribute("cancelledOrders", cancelledOrders);
        wrapper.setAttribute("disapprovedOrders", disapprovedOrders);
    }

    /**
     *
     * @param id
     * @return
     * @throws PoolException
     * @throws DAOException
     */
    public static Order setOrderDoneDate(int id) throws PoolException, DAOException {
        Calendar calendar = Calendar.getInstance();
        IOrderDAO odao = df.getOrderDao();
        Order order = odao.getById(id);
        java.util.Date currentDate = calendar.getTime();
        Date date = new Date(currentDate.getTime());
        order.setDoneOrderDate(date);
        Time time = new Time(currentDate.getTime());
        order.setDoneOrderTime(time);
        order.setOrderStatus("done");
        return order;
    }

    /**
     *
     * @param id
     * @return
     * @throws PoolException
     * @throws DAOException
     */
    public static Order setOrderDisapproved(int id) throws PoolException, DAOException {
        IOrderDAO odao = df.getOrderDao();
        Order order = odao.getById(id);
        order.setOrderStatus("disapproved");
        return order;
    }

    /**
     *
     * @param id
     * @return
     * @throws PoolException
     * @throws DAOException
     */
    public static Order setOrderApproved(int id) throws PoolException, DAOException {
        IOrderDAO odao = df.getOrderDao();
        Order order = odao.getById(id);
        order.setOrderStatus("approved");
        return order;
    }

    private static List<Order> setUsersForOrders() throws DAOException, PoolException {
        IOrderDAO dao = df.getOrderDao();
        List<Order> orders =dao.getAll();
        IUserDAO uDao = df.getUserDao();
        List<User> users = uDao.getAll();
        orders.stream().forEach((order) -> {
            for(User user: users){
                if(Objects.equals(order.getUser().getId(), user.getId())){
                    order.setUser(user);
                   
                }
            }
        });
        return orders;
    }

}
