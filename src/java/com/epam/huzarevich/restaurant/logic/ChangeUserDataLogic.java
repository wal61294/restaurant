/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.logic;

import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;

/**
 *
 * @author al94
 */
public class ChangeUserDataLogic {
    
    public static void setDataForChangeName(SessionWrapper sessionWrapper) {
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        sessionWrapper.setAttribute("name", user.getName());
        sessionWrapper.setAttribute("surname", user.getSurname());
    }
    
    public static void setDataForChangePhone(SessionWrapper sessionWrapper) {
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        sessionWrapper.setAttribute("phone", user.getPhone());
    }
    
    public static void setDataForChangeLocation(SessionWrapper sessionWrapper) {
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        sessionWrapper.setAttribute("apartment", user.getApartment());
        sessionWrapper.setAttribute("house", user.getHouse());
        sessionWrapper.setAttribute("street", user.getStreet());
    }
    
    public static User getUserWithChangedName(SessionWrapper sessionWrapper) {
        String name = sessionWrapper.getParameter("name");
        String surname = sessionWrapper.getParameter("surname");
        
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        
        if (ValidateParameterLogic.checkNameSurname(name)) {
            user.setName(name);
        } else {
            sessionWrapper.setAttribute("name", name);
            sessionWrapper.setAttribute("nameError", "Wrong name format");
            
        }
        if (ValidateParameterLogic.checkNameSurname(surname)) {
            user.setSurname(surname);
        } else {
            sessionWrapper.setAttribute("surname", surname);
            sessionWrapper.setAttribute("surnameError", "Wrong surname format");
        }
        return user;
    }
    
    public static User getUserWithChangedLocation(SessionWrapper sessionWrapper) {
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        String street = sessionWrapper.getParameter("street");
        String house = sessionWrapper.getParameter("house");
        String apartmentString = sessionWrapper.getParameter("apartment");
        if (apartmentString.length() > 0) {
            if (ValidateParameterLogic.checkApartment(apartmentString)) {
                int apartment = Integer.parseInt(apartmentString);
                user.setApartment(apartment);
            } else {
                sessionWrapper.setAttribute("apartment", apartmentString);
                sessionWrapper.setAttribute("apartmentError", "Wrong apartment format");
                user.setApartment(null);
            }
        } else {
            user.setApartment(0);
        }
        if (ValidateParameterLogic.checkHouse(house)) {
            user.setHouse(house);
        } else {
            sessionWrapper.setAttribute("house", house);
            sessionWrapper.setAttribute("houseError", "Wrong house format");
            
        }
        if (ValidateParameterLogic.checkStreet(street)) {
            user.setStreet(street);
        } else {
            sessionWrapper.setAttribute("street", street);
            sessionWrapper.setAttribute("streetError", "Wrong street format");
            
        }
        
        return user;
    }
    
}
