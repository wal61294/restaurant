/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.logic;

import com.epam.huzarevich.restaurant.entities.Product;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**Sets number of every product in order and then sets in on the page
 *
 * @author al94
 */
public class ViewOrderLogic {

    public static Map<Product, Integer> getProductsWithTheirsNumbers(List<Product> products) {
        int j = 1;
        Product previousProduct = new Product();
        Map<Product, Integer> productNumber = new HashMap<>();
        for (int i = 0; i < products.size(); i++) {
            if (i == 0) {
                previousProduct = products.get(0);
                j = 0;
            }
            if (Objects.equals(products.get(i).getId(), previousProduct.getId())) {
                previousProduct = products.get(i);
                j = j + 1;
            } else {
                productNumber.put(previousProduct, j);
                previousProduct = products.get(i);
                j = 1;
            }
            if (i == products.size() - 1) {
                productNumber.put(previousProduct, j);
            }
        }
        return productNumber;
    }
}
