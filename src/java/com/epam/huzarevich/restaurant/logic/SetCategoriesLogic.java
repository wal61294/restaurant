/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.logic;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductCategoryDAO;
import com.epam.huzarevich.restaurant.entities.ProductCategory;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author al94
 */
public class SetCategoriesLogic {
    private static DAOFactory df=DAOFactory.getDAOFactory(DAOEnum.MYSQL);

    /**Sets categories foes for product updation     *
     * @param wrapper
     * @param category
     * @throws PoolException
     * @throws DAOException
     */
    public static void setCategoriesForUpdateProduct(SessionWrapper wrapper,String category) throws PoolException, DAOException {
        IProductCategoryDAO pcDAO = df.getProductCategoryDao();
        List<ProductCategory> categories = pcDAO.getAll();
        Iterator <ProductCategory>it=categories.iterator();
        while(it.hasNext()){
            if(it.next().getName().equals(category)){
                it.remove();
            }
        }
        wrapper.setAttribute("productCategories", categories);

    }

}
