/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.logic;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IOrderDAO;
import com.epam.huzarevich.restaurant.entities.Order;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;

/**Logic for cancel order .Updates order in db and sets cancelled status;
 *
 * @author al94
 */
public class CancelOrderLogic {
    private static final DAOFactory DF=DAOFactory.getDAOFactory(DAOEnum.MYSQL);

    public static boolean cancelOrder(Order order) throws PoolException, DAOException {
        IOrderDAO oDAO = DF.getOrderDao();
        order.setOrderStatus("cancelled");
        return oDAO.update(order);

    }
}
