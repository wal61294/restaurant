/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.logic;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;

/**
 *
 * @author al94
 */
public class RegisterLogic {

    public static void setDataForReRegister(SessionWrapper sessionWrapper, User user) {
        sessionWrapper.setAttribute("phone", user.getPhone());
        sessionWrapper.setAttribute("name", user.getName());
        sessionWrapper.setAttribute("surname", user.getSurname());
        sessionWrapper.setAttribute("login", user.getLogin());
        sessionWrapper.setAttribute("street", user.getStreet());
        sessionWrapper.setAttribute("house", user.getHouse());
        sessionWrapper.setAttribute("apartment", user.getApartment());
        sessionWrapper.setAttribute("password", user.getPassword());
    }

    public static User getAndValidateUser(SessionWrapper sessionWrapper) {
        String name = sessionWrapper.getParameter("name");
        String surname = sessionWrapper.getParameter("surname");
        String login = sessionWrapper.getParameter("login");
        String street = sessionWrapper.getParameter("street");
        String house = sessionWrapper.getParameter("house");
        String apartmentString = sessionWrapper.getParameter("apartment");
        String phoneString = sessionWrapper.getParameter("phone");
        String password = sessionWrapper.getParameter("password");
        User user = new User();
        user.setRole("client");
        if (ValidateParameterLogic.checkPassword(password)) {
            user.setPassword(password);
        } else {
            sessionWrapper.setAttribute("password", password);
            sessionWrapper.setAttribute("passwordError", "Wrong password format");
        }
        if (apartmentString.length() > 0) {
            if (ValidateParameterLogic.checkApartment(apartmentString)) {
                int apartment = Integer.parseInt(apartmentString);
                user.setApartment(apartment);
            } else {
                sessionWrapper.setAttribute("apartment", apartmentString);
                sessionWrapper.setAttribute("apartmentError", "Wrong apartment format");
                user.setPassword(null);
            }

        }

        if (ValidateParameterLogic.checkHouse(house)) {
            user.setHouse(house);
        } else {
            sessionWrapper.setAttribute("house", house);
            sessionWrapper.setAttribute("houseError", "Неправильный формат дома");
            user.setPassword(null);
        }

        if (ValidateParameterLogic.checkLogin(login)) {
            user.setLogin(login);
        } else {
            sessionWrapper.setAttribute("login", login);
            sessionWrapper.setAttribute("loginError", "Неправильный формат логина");
            user.setPassword(null);
        }
        if (ValidateParameterLogic.checkNameSurname(name)) {
            user.setName(name);
        } else {
            sessionWrapper.setAttribute("name", name);
            sessionWrapper.setAttribute("nameError", "Неправильный формат имени");
            user.setPassword(null);
        }
        if (ValidateParameterLogic.checkNameSurname(surname)) {
            user.setSurname(surname);
        } else {
            sessionWrapper.setAttribute("surname", surname);
            sessionWrapper.setAttribute("surnameError", "Неправильный формат фамилии");
            user.setPassword(null);
        }
        if (ValidateParameterLogic.checkStreet(street)) {
            user.setStreet(street);
        } else {
            sessionWrapper.setAttribute("street", street);
            sessionWrapper.setAttribute("streetError", "Неправильный формат улицы");
            user.setPassword(null);
        }
        if (ValidateParameterLogic.checkPhone(phoneString)) {
            user.setPhone(Integer.parseInt(phoneString));
        } else {
            sessionWrapper.setAttribute("phone", phoneString);
            sessionWrapper.setAttribute("phoneError", "Неправильный формат телефона");
            user.setPassword(null);
        }
        return user;
    }

    public static boolean checkIfUserExists(User user) throws DAOException, PoolException {
        boolean tf = false;
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IUserDAO userdao = df.getUserDao();
        if (userdao.findByLogin(user.getLogin()) != null) {
            tf = true;
        }
        return tf;
    }

}
