/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.wrapper;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**Session wrapper class
 *
 * @author al94
 */
public class SessionWrapper {

    private Map<String, String[]> parameterMap = new LinkedHashMap<>();
    private Map<String, Object> attributeMap = new LinkedHashMap<>();
    private Map<String, Object> sessionMap = new LinkedHashMap<>();
    private HttpSession session = null;

    /**Unpacking se
     *
     * @param request
     */
    public void unpacking(HttpServletRequest request) {
        String attributeName;
        String sessionAttributeName;
        session = request.getSession(true);
        Enumeration<String> sessionAttributeNames = session.getAttributeNames();
        while (sessionAttributeNames.hasMoreElements()) {
            sessionAttributeName = sessionAttributeNames.nextElement();
            sessionMap.put(sessionAttributeName, session.getAttribute(sessionAttributeName));
        }
        parameterMap.putAll(request.getParameterMap());
        Enumeration<String> attributeNames = request.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            attributeName = attributeNames.nextElement();
            attributeMap.put(attributeName, request.getAttribute(attributeName));
        }
    }

    /**Updates request 
     *
     * @param request
     * @return
     */
    public HttpServletRequest updateRequest(HttpServletRequest request) {
        String attributeName;
        String sessionAttributeName;
        session = request.getSession(true);
        Set<String> attributeNames = attributeMap.keySet();
        Iterator<String> attributeNamesIterator = attributeNames.iterator();
        while (attributeNamesIterator.hasNext()) {
            attributeName = attributeNamesIterator.next();
            request.setAttribute(attributeName, attributeMap.get(attributeName));
        }
        Set<String> sessionAttributeNames = sessionMap.keySet();
        Iterator<String> sessionAttributeNamesIterator = sessionAttributeNames.iterator();
        while (sessionAttributeNamesIterator.hasNext()) {
            sessionAttributeName = sessionAttributeNamesIterator.next();
            session.setAttribute(sessionAttributeName, sessionMap.get(sessionAttributeName));
        }
        return request;
    }

    /**Gets parameter from session
     *
     * @param key
     * @return
     */
    public String getParameter(String key) {
        return parameterMap.get(key)[0];
    }

    /**Sets attribute 
     *
     * @param key
     * @param value
     */
    public void setAttribute(String key, Object value) {
        attributeMap.put(key, value);
    }

    /**Sets attribute to session
     *
     * @param key
     * @param value
     */
    public void setSessionAttribute(String key, Object value) {
        sessionMap.put(key, value);
    }

    /**
     *
     * @param key
     * @return
     */
    public Object getSessionAttribute(String key) {
        return sessionMap.get(key);
    }

    /**
     *
     */
    public void removeSession() {
        this.session.invalidate();
    }

}
