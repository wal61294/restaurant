package com.epam.huzarevich.restaurant.connection_pool;

import com.epam.huzarevich.restaurant.exceptions.PoolException;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.ResourceBundle;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;

/**
 * Class implements db connection pool .
 * <p>
 * Provides connections to users, quantity of which is limited.
 * </p>
 *
 * @author huz
 */
public final class DBConnectionPool {

    private static ResourceBundle resource = ResourceBundle.getBundle("properties.database");
    private static final String URL = resource.getString("db.url");
    private static final String USER = resource.getString("db.user");
    private static final String PASSWORD = resource.getString("db.password");
    private static DBConnectionPool instance;
    private static final int size = Integer.parseInt(resource.getString("db.poolsize"));
    private static final Lock lock = new ReentrantLock();
    private Logger logger = Logger.getLogger(this.getClass());
    private BlockingQueue<Connection> connections = new ArrayBlockingQueue<>(10, true);

    /**
     * Singleton method returns DBConnectionPool 
     * @return
     * @throws SQLException
     * @throws PoolException
     */
    public static DBConnectionPool getInstance() throws SQLException, PoolException {
        lock.lock();
        try {
            if (instance == null) {
                instance = new DBConnectionPool();
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }

    private DBConnectionPool() throws PoolException {
        try {
            registerDriver();
        } catch (Exception e) {
            PoolException pool = new PoolException(e.getMessage());
            pool.setPropertyMessage("PoolException");
            logger.error(e.getMessage());
            throw pool;
        }
        for (int i = 0; i < size; i++) {
            try {
                connections.add(DriverManager.getConnection(URL, USER, PASSWORD));
            } catch (SQLException e) {
                PoolException pool = new PoolException(e.getMessage());
                pool.setPropertyMessage("PoolException");
                logger.error(e.getMessage());
                throw pool;
            }
        }
    }

    /**
     * returns connection from DB.
     * @return
     * @throws PoolException
     */
    public Connection getConnection() throws PoolException {
        Connection connection = null;
        try {
            connection = connections.take();
        } catch (InterruptedException e) {
            PoolException pool = new PoolException(e.getMessage());
            pool.setPropertyMessage("PoolException");
            logger.error(e.getMessage());
            throw pool;
        }
        return connection;
    }

    /**
     *Closes DB connection
     * @param connection
     * @throws PoolException
     */
    public void closeConnection(Connection connection) throws PoolException {
        if (connection != null) {
            try {
                connections.put(connection);
            } catch (InterruptedException e) {
                PoolException pool = new PoolException(e.getMessage());
                pool.setPropertyMessage("PoolException");
                logger.error(e.getMessage());
                throw pool;
            }
        }
    }

    /**Register and Deregister  MySQL Driver
     *
     * @throws PoolException
     */
    public void registerDriver() throws PoolException {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            PoolException pool = new PoolException(e.getMessage());
            pool.setPropertyMessage("Couldn't register driver");
            logger.error(e.getMessage());
            throw pool;
        }
    }
    private void deregisterDriver() throws PoolException {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while(drivers.hasMoreElements()){
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
            } catch (SQLException e) {
                PoolException pool = new PoolException(e.getMessage());
                pool.setPropertyMessage("Couldn't deregister driver: " + driver);
                logger.error(e.getMessage());
            throw pool;
            }
        }
    }

    /**Closes all connections and deregister MYSQL Driver
     *
     * @throws PoolException
     */
    public void releasePool() throws PoolException{
        while (!connections.isEmpty()) {
            Connection connection;
            if ((connection = connections.poll()) != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error("Error when releasing pool", e);
                }
            }
        }
        deregisterDriver();
    }
}
