/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.resourse;

import java.util.ResourceBundle;

/**Allows to get data from properties.config to configure DB and pages
 *
 * @author al94
 */
public class ConfigurationManager {

    private final static ResourceBundle resourceBundle
            = ResourceBundle.getBundle("properties.config");

    private ConfigurationManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
