/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.resourse;

import java.util.ResourceBundle;

/**Allows to get messages for i18n
 *
 * @author al94
 */
public class MessageManager {

    private final static ResourceBundle resourceBundle
            = ResourceBundle.getBundle("properties.messages");

    private MessageManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
