/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.entities;

import java.util.List;
import java.util.Objects;

/**User extens Entity represents Mysql tables user user_role and links to orders
 *
 * @author al94
 */
public class User extends Entity {

    private String name;
    private String surname;
    private String login;
    private String password;
    private String roleName;
    private String street;
    private String house;
    private Integer apartment = 0;
    private int phone;

    private List<Order> orders;

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getSurname() {
        return surname;
    }

    public String getRole() {
        return roleName;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setRole(String roleName) {
        this.roleName = roleName;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getApartment() {
        return apartment;
    }

    public void setApartment(Integer room) {
        this.apartment = room;
    }

    @Override
    public boolean equals(Object obj) {
        User inspectedObj = (User) obj;
        if (inspectedObj == null) {
            return false;
        } else if (!getApartment().equals(inspectedObj.getApartment())) {
            return false;
        } else if (!getHouse().equals(inspectedObj.getHouse())) {
            return false;
        } else if (!getLogin().equals(inspectedObj.getLogin())) {
            return false;
        } else if (!getSurname().equals(inspectedObj.getSurname())) {
            return false;
        } else if (!getStreet().equals(inspectedObj.getStreet())) {
            return false;
        } else if (!getName().equals(inspectedObj.getName())) {
            return false;
        } else if (!getPassword().equals(inspectedObj.getPassword())) {
            return false;
        } else if (!getOrders().equals(inspectedObj.getOrders())) {
            return false;
        } else if (!getRole().equals(inspectedObj.getRole())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * getId();
        hash = 53 * hash + Objects.hashCode(getName());
        hash = 53 * hash + Objects.hashCode(getSurname());
        hash = 53 * hash + Objects.hashCode(getLogin());
        hash = 53 * hash + Objects.hashCode(getPassword());
        hash = 53 * hash + Objects.hashCode(getRole());
        hash = 53 * hash + Objects.hashCode(getStreet());
        hash = 53 * hash + Objects.hashCode(getApartment());
        hash = 53 * hash + getPhone();
        hash = 53 * hash + Objects.hashCode(getOrders());
        return hash;
    }

    @Override
    public String toString() {
        return name + " " + surname + ", login :" + login + ", role:" + roleName
                + ", adress:" + street + "street  " + house + ".";
    }

}
