package com.epam.huzarevich.restaurant.entities;

import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.Objects;

/**User extens Entity represents Mysql tables orders , order_status, 
 * order_products  and links to products and users
 *
 * @author al94
 */
public class Order extends Entity {

    private Date orderDate;
    private Date doneOrderDate = null;
    private Time orderTime;
    private Time doneOrderTime = null;

    private User user;
    private List<Product> products;
    private int price;
    private String orderStatus;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> productIds) {
        this.products = productIds;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Date getDoneOrderDate() {
        return doneOrderDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setDoneOrderDate(Date doneOrderDate) {
        this.doneOrderDate = doneOrderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Time getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Time orderTime) {
        this.orderTime = orderTime;
    }

    public Time getDoneOrderTime() {
        return doneOrderTime;
    }

    public void setDoneOrderTime(Time doneOrderTime) {
        this.doneOrderTime = doneOrderTime;
    }

    @Override
    public boolean equals(Object obj) {
        Order inspectedObj = (Order) obj;
        if (inspectedObj == null) {
            return false;
        } else if (!Objects.equals(inspectedObj.getId(), getId())) {
            return false;
        } else if (!inspectedObj.getOrderStatus().equals(getOrderStatus())) {
            return false;
        }
        return true;
    }


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * getId();
        hash = 97 * hash + (getOrderDate() == null ? 0 : getOrderDate().hashCode());
        hash = 97 * hash + (getDoneOrderDate() == null ? 0 : getDoneOrderDate().hashCode());
        hash = 97 * hash + (getOrderTime() == null ? 0 : getOrderTime().hashCode());
        hash = 97 * hash + (getDoneOrderTime() == null ? 0 : getDoneOrderTime().hashCode());
        hash = 97 * hash + (getUser() == null ? 0 : getUser().hashCode());
        hash = 97 * hash + (getProducts() == null ? 0 : getProducts().hashCode());
        hash = 97 * hash + getPrice();
        hash = 97 * hash + (getOrderStatus() == null ? 0 : getOrderStatus().hashCode());
        return hash;
    }

}
