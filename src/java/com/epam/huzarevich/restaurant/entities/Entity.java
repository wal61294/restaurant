
package com.epam.huzarevich.restaurant.entities;

/**Entity Abstract class
 *
 * @author al94
 */
public abstract class Entity {

    private int id;

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
