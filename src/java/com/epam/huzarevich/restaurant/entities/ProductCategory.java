/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.entities;

import java.util.List;
import java.util.Objects;

/**User extens Entity represents Mysql table product category  and links to product
 *
 * @author al94
 */
public class ProductCategory extends Entity {

    private String name;
    private List<Product> products;
    private boolean visible;

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object obj) {
        ProductCategory inspectedObj = (ProductCategory) obj;

        if (inspectedObj == null) {
            return false;
        } else if (!getName().equals(inspectedObj.getName())) {
            return false;
        } else if (isVisible() != inspectedObj.isVisible()) {
            return false;
        } else if (!getProducts().equals(inspectedObj.getProducts())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * getId();
        hash = 97 * hash + Objects.hashCode(getName());
        hash = 97 * hash + Objects.hashCode(getProducts());
        hash = 97 * hash + (isVisible() ? 1 : 0);
        return hash;
    }

}
