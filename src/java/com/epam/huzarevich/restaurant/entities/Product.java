/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.entities;

/**User extens Entity represents Mysql table product   and links to productcategory
 *
 * @author al94
 */
public class Product extends Entity {

    private String name;
    private int price;
    private ProductCategory category;
    private int mass;
    private boolean inMenu;

    public int getMass() {
        return mass;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isInMenu() {
        return inMenu;
    }

    public void setIsInMenu(boolean isInMenu) {
        this.inMenu = isInMenu;
    }

    @Override
    public boolean equals(Object obj) {
        Product inspectedObj = (Product) obj;
        if (inspectedObj == null) {
            return false;
        } else if (isInMenu() != inspectedObj.isInMenu()) {
            return false;
        } else if (!getName().equals(inspectedObj.getName())) {
            return false;
        } else if (getMass() != inspectedObj.getMass()) {
            return false;
        } else if (!getCategory().equals(inspectedObj.getCategory())) {
            return false;
        } else if (getPrice() != inspectedObj.getPrice()) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * getId();
        hash = 97 * hash + (getName() == null ? 0 : getName().hashCode());
        hash = 97 * hash + getPrice();
        hash = 97 * hash + (getCategory() == null ? 0 : getCategory().hashCode());
        hash = 97 * hash + getMass();
        hash = 97 * hash + (isInMenu() ? 1 : 0);
        return hash;
    }

    public String toString() {
        return name + " mass : " + mass + ",price :" + price + ".";
    }

}
