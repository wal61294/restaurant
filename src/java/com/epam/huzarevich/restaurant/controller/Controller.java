/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.controller;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.action.factory.ActionFactory;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 



/**
 *Restaurants' controller
 * @author al94
 */
public class Controller extends HttpServlet {
    
    private static final long serialVersionUID = 1L;
    private ActionFactory factory;
    
    /**Initialises Action Factory
     *
     */
    @Override
    public void init() {
        factory = new ActionFactory();
        
    }

    /**Overrided service
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String page = null;
        SessionWrapper sessionWrapper = new SessionWrapper();
        sessionWrapper.unpacking(req);
        Action action = factory.getAction(sessionWrapper);
        page = action.execute(sessionWrapper);
        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            req = sessionWrapper.updateRequest(req);
            dispatcher.forward(req, resp);
        } else {
            page = ConfigurationManager.getProperty("path.error");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            req = sessionWrapper.updateRequest(req);
            dispatcher.forward(req, resp);
        }
    }

}
