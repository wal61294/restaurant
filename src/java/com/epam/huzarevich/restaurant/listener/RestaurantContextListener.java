/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.listener;

import com.epam.huzarevich.restaurant.connection_pool.DBConnectionPool;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *needed for doing some things in the end of app work and at the start
 * @author al94
 */
public class RestaurantContextListener implements ServletContextListener {

    private final Logger logger = Logger.getLogger(this.getClass());

    /**Initialises Connection pool and log4g at the beginning of app
     *
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        PropertyConfigurator.configure(ConfigurationManager.getProperty("log4j.path"));
        try {
            DBConnectionPool pool = DBConnectionPool.getInstance();
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
        } catch (PoolException ex) {
            logger.error(ex.getMessage());
        }
    }

    /**Releases Connection pool  it the end of app
     *
     * @param sce
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            DBConnectionPool.getInstance().releasePool();
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
        } catch (PoolException ex) {
            logger.error(ex.getMessage());
        }
    }

}
