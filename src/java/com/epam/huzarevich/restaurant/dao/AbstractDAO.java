package com.epam.huzarevich.restaurant.dao;

import com.epam.huzarevich.restaurant.connection_pool.DBConnectionPool;
import com.epam.huzarevich.restaurant.entities.Entity;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import java.sql.Connection;
import java.sql.SQLException;

/**Abstract DAO
 *
 * @author al94
 * @param <T>
 */
public abstract class AbstractDAO<T extends Entity> {

    /**
     *
     */
    protected Connection connection;

    /** gets Connsection from DBConnectionPool
     *
     * @return Connection
     * @throws PoolException
     */
    public Connection getConnection() throws PoolException {
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            return connection;
        } catch (SQLException ex) {
            PoolException pool = new PoolException(ex.getMessage());
            throw pool;
        }
    }

    /** close Connsection from DBConnectionPool
     *
     * @throws PoolException
     */
    public void closeConnection() throws PoolException {
        if (connection != null) {
            try {
                DBConnectionPool.getInstance().closeConnection(connection);
            } catch (SQLException ex) {
                PoolException pool = new PoolException(ex.getMessage());
                throw pool;
            }
        }
    }
}
