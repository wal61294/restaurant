package com.epam.huzarevich.restaurant.dao.mysql_impl;

import com.epam.huzarevich.restaurant.connection_pool.DBConnectionPool;
import com.epam.huzarevich.restaurant.dao.AbstractDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductDAO;
import com.epam.huzarevich.restaurant.entities.Product;
import com.epam.huzarevich.restaurant.entities.ProductCategory;
import com.epam.huzarevich.restaurant.exceptions.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author al94
 */
public class ProductDAOMySQLImpl extends AbstractDAO<Product> implements IProductDAO {

    private final Logger logger = Logger.getLogger(this.getClass());

    private final static String GET_PRODUCT_BY_ID_QUERY = "SELECT * FROM restaurant.product "
            + "INNER JOIN restaurant.product_category ON restaurant.product.product_category_id = restaurant.product_category.id "
            + "WHERE restaurant.product.id=?;";
    private final static String GET_ALL_PRODUCTS_QUERY = "SELECT * FROM restaurant.product "
            + "INNER JOIN restaurant.product_category ON restaurant.product.product_category_id = restaurant.product_category.id;";
    private final static String ADD_PRODUCT_QUERY = "INSERT INTO `restaurant`.`product` (`name`, `price`, `product_category_id`, `mass`) VALUES ( ?, ?, ?, ?)";
    private final static String GET_PRODUCTS_BY_ORDERID_QUERY = "SELECT * FROM restaurant.order_products \n"
            + "inner join restaurant.product\n"
            + "on restaurant.order_products.product_id= restaurant.product.id "
            + "INNER JOIN restaurant.product_category"
            + " ON restaurant.product.product_category_id=restaurant.product_category.id "
            + "  where orders_id=?;";
    private final static String UPDATE_PRODUCT_QUERY = "UPDATE restaurant.product SET product.name=?,"
            + " product.mass=?, product.price=?, product.product_category_id=?, "
            + "product.is_in_menu=? WHERE product.id=?";

    protected ProductDAOMySQLImpl() {

    }

    @Override
    public Product getById(int id) throws DAOException, PoolException {

        Product product = null;
        ProductCategory category = new ProductCategory();
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            PreparedStatement st = connection.prepareStatement(GET_PRODUCT_BY_ID_QUERY);
            st.setInt(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                product = new Product();
                product.setId(resultSet.getInt("id"));
                product.setName(resultSet.getString("name"));
                product.setPrice(resultSet.getInt("price"));
                category.setName(resultSet.getString("product_category.name"));
                category.setId(resultSet.getInt("product_category_id"));
                product.setCategory(category);
                product.setMass(resultSet.getInt("mass"));
                int isInMenuInt = resultSet.getInt("is_in_menu");
                boolean isInMenu = false;
                if (isInMenuInt == 1) {
                    isInMenu = true;
                }
                product.setIsInMenu(isInMenu);

            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return product;
    }

    @Override
    public List<Product> getAll() throws DAOException, PoolException {
        Statement st = null;

        List<Product> products = new ArrayList<>();
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            st = connection.createStatement();
            ResultSet resultSet = st.executeQuery(GET_ALL_PRODUCTS_QUERY);
            while (resultSet.next()) {
                Product product = new Product();
                product.setId(resultSet.getInt("id"));
                product.setName(resultSet.getString("name"));
                product.setPrice(resultSet.getInt("price"));
                ProductCategory category = new ProductCategory();
                category.setName(resultSet.getString("product_category.name"));
                category.setId(resultSet.getInt("product_category_id"));
                product.setCategory(category);
                product.setMass(resultSet.getInt("mass"));
                int isInMenuInt = resultSet.getInt("is_in_menu");
                boolean isInMenu = false;
                if (isInMenuInt == 1) {
                    isInMenu = true;
                }
                product.setIsInMenu(isInMenu);
                products.add(product);
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return products;
    }

    @Override
    public boolean add(Product t) throws DAOException, PoolException {
        Product product = (Product) t;

        boolean result = true;
        PreparedStatement ps = null;
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            ps = connection.prepareStatement(ADD_PRODUCT_QUERY);
            ps.setString(1, product.getName());
            ps.setInt(2, product.getPrice());
            ps.setInt(4, product.getMass());

            ps.setInt(3, product.getCategory().getId());
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return result;
    }

    @Override
    public List<Product> getProductsByOrderId(int id) throws DAOException, PoolException {

        List<Product> products = new ArrayList<>();
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            PreparedStatement st = connection.prepareStatement(GET_PRODUCTS_BY_ORDERID_QUERY);
            st.setInt(1, id);
            ResultSet rsL = st.executeQuery();
            while (rsL.next()) {
                ProductCategory category = new ProductCategory();
                Product product = new Product();
                product.setId(rsL.getInt("product.id"));
                product.setName(rsL.getString("name"));
                product.setPrice(rsL.getInt("price"));
                product.setMass(rsL.getInt("mass"));
                category.setName(rsL.getString("product_category.name"));
                category.setId(rsL.getInt("product_category_id"));
                product.setCategory(category);
                products.add(product);
            }

        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return products;
    }

    @Override
    public boolean update(Product t) throws DAOException, PoolException {
        boolean result = true;
        PreparedStatement ps = null;
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            ps = connection.prepareStatement(UPDATE_PRODUCT_QUERY);
            ps.setString(1, t.getName());
            ps.setInt(2, t.getMass());
            ps.setInt(3, t.getPrice());
            int isInMenu = 0;
            if (t.isInMenu()) {
                isInMenu = 1;
            }
            ps.setInt(4, t.getCategory().getId());
            ps.setInt(5, isInMenu);
            ps.setInt(6, t.getId());
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }

        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return result;
    }

}
