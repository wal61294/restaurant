/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.dao.mysql_impl;

import com.epam.huzarevich.restaurant.connection_pool.DBConnectionPool;
import com.epam.huzarevich.restaurant.dao.AbstractDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.entities.Entity;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author al94
 */
public class UserDAOMySQLImpl extends AbstractDAO<User> implements IUserDAO {

    private final Logger logger = Logger.getLogger(this.getClass());
    private final static String GET_USER_BY_ID_QUERY = "SELECT * FROM restaurant.user "
            + "INNER JOIN restaurant.user_role "
            + "ON restaurant.user.user_role_id = restaurant.user_role.id where restaurant.user.id=?;";
    private final static String GET_USER_BY_LOGIN_QUERY = "SELECT * FROM restaurant.user "
            + "INNER JOIN restaurant.user_role"
            + " ON restaurant.user.user_role_id = restaurant.user_role.id where restaurant.user.login=?;";
    private final static String GET_ALL_USERS_QUERY = "SELECT * FROM restaurant.user "
            + "INNER JOIN restaurant.user_role"
            + " ON restaurant.user.user_role_id = restaurant.user_role.id;";
    private final static String ADD_USER_QUERY = "INSERT INTO `restaurant`.`user` "
            + "(`login`, `name`, `surname`, `password`, `user_role_id`, street, house, apartment, phone) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
     private final static String UPDATE_USER_QUERY="UPDATE `restaurant`.`user`"
                    + " SET `name`=?, surname=?, `password`=?, `street`=?, `house`=?, `apartment`=?, `phone`=? "
                    + "WHERE `id`=?; ";

    protected UserDAOMySQLImpl() {

    }

    @Override
    public User getById(int id) throws DAOException, PoolException {
        PreparedStatement st = null;
        User user = null;
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            st = connection.prepareStatement(GET_USER_BY_ID_QUERY);
            st.setInt(1, id);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setRole(resultSet.getString("user_role.name"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setHouse(resultSet.getString("house"));
                user.setStreet(resultSet.getString("street"));
                user.setApartment(resultSet.getInt("apartment"));
                user.setPhone(resultSet.getInt("phone"));
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return user;
    }

    @Override
    public List getAll() throws DAOException, PoolException {
        Statement st = null;
        List<User> users = new ArrayList<>();
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            st = connection.createStatement();
            ResultSet resultSet = st.executeQuery(GET_ALL_USERS_QUERY);
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setRole(resultSet.getString("user_role.name"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setHouse(resultSet.getString("house"));
                user.setStreet(resultSet.getString("street"));
                user.setApartment(resultSet.getInt("apartment"));
                user.setPhone(resultSet.getInt("phone"));
                users.add(user);
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return users;
    }

    @Override
    public User findByLogin(String parameter) throws DAOException, PoolException {
        PreparedStatement st = null;
        User user = null;

        try {
            connection = DBConnectionPool.getInstance().getConnection();
            st = connection.prepareStatement(GET_USER_BY_LOGIN_QUERY);
            st.setString(1, parameter);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setRole(resultSet.getString("user_role.name"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setHouse(resultSet.getString("house"));
                user.setStreet(resultSet.getString("street"));
                user.setApartment(resultSet.getInt("apartment"));
                user.setPhone(resultSet.getInt("phone"));

            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return user;
    }

    @Override
    public boolean add(User t) throws DAOException, PoolException {
        User user = (User) t;
        boolean result = true;
        PreparedStatement ps = null;
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            ps = connection.prepareStatement(ADD_USER_QUERY);
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getName());
            ps.setString(3, user.getSurname());
            ps.setString(4, user.getPassword());
            ps.setString(6, user.getStreet());
            ps.setString(7, user.getHouse());
            if (user.getApartment() != 0) {
                ps.setInt(8, user.getApartment());
            } else {
                ps.setNull(8, java.sql.Types.INTEGER);
            }

            ps.setInt(9, user.getPhone());
            if (user.getRole().equals("client")) {
                ps.setInt(5, 1);
            } else if (user.getRole().equals("admin")) {
                ps.setInt(5, 2);
            }
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return result;
    }

    @Override
    public boolean update(User t) throws DAOException, PoolException {
        boolean result = true;
        PreparedStatement ps = null;
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            ps = connection.prepareStatement(UPDATE_USER_QUERY);

            ps.setString(1, t.getName());
            ps.setString(2, t.getSurname());
            ps.setString(3, t.getPassword());
            ps.setString(4, t.getStreet());
            ps.setString(5, t.getHouse());
            ps.setInt(6, t.getApartment());
            ps.setInt(7, t.getPhone());
            ps.setInt(8, t.getId());

            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return result;

    }

}
