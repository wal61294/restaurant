/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.dao.mysql_impl;

import com.epam.huzarevich.restaurant.connection_pool.DBConnectionPool;
import com.epam.huzarevich.restaurant.dao.AbstractDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IOrderDAO;
import com.epam.huzarevich.restaurant.entities.Order;
import com.epam.huzarevich.restaurant.entities.Product;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author al94
 */
public class OrderDAOMySQLImpl extends AbstractDAO<Order> implements IOrderDAO {

    private final Logger logger = Logger.getLogger(this.getClass());
    private static final String GET_ORDER_WITH_ORDER_STATUS_QUERY = "SELECT * FROM restaurant.orders INNER JOIN restaurant.order_status "
            + "ON restaurant.orders.order_status_id=restaurant.order_status.id "
            + "WHERE restaurant.orders.id=?;";
    private static final String GET_ORDER_PRODUCTS_BY_ID_QUERY = "SELECT * FROM restaurant.order_products \n"
            + "inner join restaurant.product\n"
            + "on restaurant.order_products.product_id= restaurant.product.id "
            + "  where orders_id=?;";
    private static final String GET_ORDERS_WITH_ORDER_STATUS_QUERY = "SELECT * FROM restaurant.orders INNER JOIN restaurant.order_status "
            + "ON restaurant.orders.order_status_id=restaurant.order_status.id;";
    private static final String GET_ORDER_PRODUCTS_QUERY = "SELECT * FROM restaurant.order_products \n"
            + "inner join restaurant.product\n"
            + "on restaurant.order_products.product_id= restaurant.product.id "
            + "  where orders_id=?;";
    private static final String LAST_ORDER_ID_QUERY = "SELECT * FROM restaurant.orders ORDER BY restaurant.orders.id DESC LIMIT 1;";

    private static final String ADD_ORDER_QUERY = "INSERT INTO `restaurant`.`orders` (`id`, `order_date`, `user_id`, `order_status_id`, `order_time`) \n"
            + "VALUES ( ?, ?, ?, ?, ?);";
    private static final String PRODUCT_DETAILS_QUERY = "insert into `restaurant`.`order_products`(`product_id`,`orders_id`) values(?, ?);";

    private static final String UPDATE_PRICE_QUERY = "UPDATE restaurant.orders SET orders.price=?  WHERE orders.id=?";

    private static final String GET_ORDERS_BY_USER_ID_QUERY = "SELECT * FROM restaurant.orders INNER JOIN restaurant.order_status "
            + "ON restaurant.orders.order_status_id=restaurant.order_status.id WHERE restaurant.orders.user_id=?;";
    private static final String GET_ORDERS_BY_USER_ID_WITH_PRODUCTS_QUERY = "SELECT * FROM restaurant.order_products \n"
            + "inner join restaurant.product\n"
            + "on restaurant.order_products.product_id= restaurant.product.id "
            + "INNER JOIN restaurant.product_category"
            + "  where orders_id=?;";
    private static final String GET_LAST_ORDER_QUERY = "SELECT * FROM restaurant.orders INNER JOIN restaurant.order_status "
            + "ON restaurant.orders.order_status_id=restaurant.order_status.id;";
    private static final String GET_LAST_ORDER_WITH_PRODUCTS_QUERY="SELECT * FROM restaurant.order_products \n"
                                + "inner join restaurant.product\n"
                                + "on restaurant.order_products.product_id= restaurant.product.id "
                                + "  where orders_id=?;";
    private static final String UPDATE_ORDER_QUERY ="UPDATE restaurant.orders SET orders.done_order_date=?, orders.done_order_time=?, orders.order_status_id=?"
                    + " WHERE orders.id=?";
    
    
    protected OrderDAOMySQLImpl() {

    }

    @Override
    public Order getById(int id) throws DAOException, PoolException {

        Order order = null;

        try {
            connection = DBConnectionPool.getInstance().getConnection();
            PreparedStatement st = connection.prepareStatement(GET_ORDER_WITH_ORDER_STATUS_QUERY);
            st.setInt(1, id);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                order = new Order();
                order.setId(resultSet.getInt("id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setOrderTime(resultSet.getTime("order_time"));
                order.setDoneOrderDate(resultSet.getDate("done_order_date"));
                order.setDoneOrderTime(resultSet.getTime("done_order_time"));
                order.setOrderStatus(resultSet.getString("status_name"));
                order.setPrice(resultSet.getInt("price"));
                PreparedStatement preparedStatement
                        = connection.prepareStatement(GET_ORDER_PRODUCTS_BY_ID_QUERY);
                preparedStatement.setInt(1, order.getId());
                ResultSet rsL = preparedStatement.executeQuery();
                List<Product> products = new ArrayList<>();
                while (rsL.next()) {
                    Product product = new Product();
                    product.setId(rsL.getInt("product.id"));
                    product.setName(rsL.getString("name"));
                    product.setMass(rsL.getInt("mass"));
                    product.setPrice(rsL.getInt("price"));
                    products.add(product);
                }
                order.setProducts(products);

            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return order;
    }

    @Override
    public List getAll() throws DAOException, PoolException {
        Statement st = null;
        List<Order> orders = new ArrayList<>();
        List<Product> products = new ArrayList<>();

        try {
            connection = DBConnectionPool.getInstance().getConnection();
            st = connection.createStatement();
            ResultSet resultSet = st.executeQuery(GET_ORDERS_WITH_ORDER_STATUS_QUERY);
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt("id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setOrderTime(resultSet.getTime("order_time"));
                order.setDoneOrderDate(resultSet.getDate("done_order_date"));
                order.setDoneOrderTime(resultSet.getTime("done_order_time"));
                order.setOrderStatus(resultSet.getString("status_name"));
                order.setPrice(resultSet.getInt("price"));
                User user = new User();
                int userId = resultSet.getInt("user_id");
                user.setId(userId);
                order.setUser(user);
                PreparedStatement preparedStatement
                        = connection.prepareStatement(GET_ORDER_PRODUCTS_QUERY);
                preparedStatement.setInt(1, order.getId());
                ResultSet rsL = preparedStatement.executeQuery();

                while (rsL.next()) {
                    Product product = new Product();
                    product.setId(rsL.getInt("id"));
                    product.setName(rsL.getString("name"));
                    product.setPrice(rsL.getInt("price"));
                    products.add(product);
                }
                order.setProducts(products);
                orders.add(order);
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return orders;
    }

    @Override
    public boolean add(Order t) throws DAOException, PoolException {
        Order order = (Order) t;
        boolean result = true;
        PreparedStatement ps = null;
        List< Product> products = order.getProducts();
        Statement st = null;
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            connection.setAutoCommit(false);
            st = connection.createStatement();
            ResultSet rs = st.executeQuery(LAST_ORDER_ID_QUERY);
            int orderId = 0;
            while (rs.next()) {
                orderId = rs.getInt("id") + 1;
            }
            ps = connection.prepareStatement(ADD_ORDER_QUERY);
            ps.setInt(1, orderId);
            ps.setDate(2, order.getOrderDate());
            ps.setInt(3, order.getUser().getId());
            ps.setInt(4, 1);
            ps.setTime(5, order.getOrderTime());

            ps.execute();

            int price = 0;
            for (Product product : products) {
                ps = connection.prepareStatement(PRODUCT_DETAILS_QUERY);
                ps.setInt(2, orderId);
                ps.setInt(1, product.getId());
                price += product.getPrice();
                ps.execute();
            }
            ps = connection.prepareStatement(UPDATE_PRICE_QUERY);
            ps.setInt(1, price);
            ps.setInt(2, orderId);
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }
            connection.commit();

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return result;

    }

    @Override
    public List<Order> getAllOrdersByUserId(int userId) throws DAOException, PoolException {

        List<Order> orders = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            PreparedStatement preparedStatement
                    = connection.prepareStatement(GET_ORDERS_BY_USER_ID_QUERY);
            preparedStatement.setInt(1, userId);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt("id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setOrderTime(resultSet.getTime("order_time"));
                order.setDoneOrderDate(resultSet.getDate("done_order_date"));
                order.setDoneOrderTime(resultSet.getTime("done_order_time"));
                order.setOrderStatus(resultSet.getString("status_name"));
                order.setPrice(resultSet.getInt("price"));
                PreparedStatement preparedStatement1
                        = connection.prepareStatement(GET_ORDERS_BY_USER_ID_WITH_PRODUCTS_QUERY);
                preparedStatement1.setInt(1, order.getId());
                int price = 0;
                ResultSet rsL = preparedStatement1.executeQuery();
                while (rsL.next()) {
                    Product product = new Product();
                    product.setId(rsL.getInt("id"));
                    product.setName(rsL.getString("name"));
                    product.setPrice(rsL.getInt("price"));
                    products.add(product);
                    price += product.getPrice();
                }
                order.setProducts(products);
                order.setPrice(price);
                orders.add(order);
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return orders;
    }

    @Override
    public Order getLastOrder() throws DAOException, PoolException {
        Statement st = null;
        Order order = null;
        List<Product> products = new ArrayList<>();

        try {
            connection = DBConnectionPool.getInstance().getConnection();
            st = connection.createStatement();
            ResultSet resultSet = st.executeQuery(GET_LAST_ORDER_QUERY);
            while (resultSet.next()) {
                order = new Order();
                order.setId(resultSet.getInt("id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setOrderTime(resultSet.getTime("order_time"));
                order.setDoneOrderDate(resultSet.getDate("done_order_date"));
                order.setDoneOrderTime(resultSet.getTime("done_order_time"));
                order.setOrderStatus(resultSet.getString("status_name"));
                order.setPrice(resultSet.getInt("price"));
                PreparedStatement preparedStatement
                        = connection.prepareStatement(GET_LAST_ORDER_WITH_PRODUCTS_QUERY);
                preparedStatement.setInt(1, order.getId());
                ResultSet rsL = preparedStatement.executeQuery();

                while (rsL.next()) {
                    Product product = new Product();
                    product.setId(rsL.getInt("id"));
                    product.setName(rsL.getString("name"));
                    product.setPrice(rsL.getInt("price"));
                    products.add(product);

                }
                order.setProducts(products);

            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return order;
    }

    @Override
    public boolean update(Order t) throws DAOException, PoolException {
        boolean result = true;
        PreparedStatement ps = null;
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            ps = connection.prepareStatement(UPDATE_ORDER_QUERY);
            ps.setDate(1, t.getDoneOrderDate());
            ps.setTime(2, t.getDoneOrderTime());
            int orderStatusId = 0;
            switch (t.getOrderStatus()) {
                case "done":
                    orderStatusId = 5;
                    break;
                case "new":
                    orderStatusId = 1;
                    break;
                case "approved":
                    orderStatusId = 3;
                    break;
                case "cancelled":
                    orderStatusId = 4;
                    break;
                case "disapproved":
                    orderStatusId = 2;
                    break;
            }
            ps.setInt(3, orderStatusId);
            ps.setInt(4, t.getId());
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }

        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return result;
    }

}
