
package com.epam.huzarevich.restaurant.dao.mysql_impl;

import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.*;

/**returns MySQL Implementations
 *
 * @author al94
 */
public   class MySQLDAOFactory extends DAOFactory {

    @Override
    public IOrderDAO getOrderDao() {
        return new OrderDAOMySQLImpl();
    }

    @Override
    public IProductDAO getProductDao() {
        return new ProductDAOMySQLImpl();
    }

    @Override
    public IUserDAO getUserDao() {
        return new UserDAOMySQLImpl();
    }

    @Override
    public IProductCategoryDAO getProductCategoryDao() {
        return new ProductCategoryDAOMySQLImpl();
    }

}
