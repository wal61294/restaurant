/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.dao.mysql_impl;

import com.epam.huzarevich.restaurant.connection_pool.DBConnectionPool;
import com.epam.huzarevich.restaurant.dao.AbstractDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductCategoryDAO;
import com.epam.huzarevich.restaurant.entities.ProductCategory;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author al94
 */
public class ProductCategoryDAOMySQLImpl extends AbstractDAO<ProductCategory> implements IProductCategoryDAO {
    private final Logger logger = Logger.getLogger(this.getClass());
    private static final String GET_CATEGORIES_QUERY="SELECT * FROM restaurant.product_category;";
    private static final String FIND_BY_NAME_QUERY="SELECT * FROM restaurant.product_category where restaurant.product_category.name=?;";
    private static final String ADD_CATEGORY_QUERY="INSERT INTO `restaurant`.`product_category` (`name`) VALUES ( ?)";
    private static final String UPDATE_CATEGORY_QUERY="UPDATE `restaurant`.`product_category` SET `name`=?, is_visible=? WHERE `id`=?;";
    protected ProductCategoryDAOMySQLImpl(){
        
    }
    @Override
    public ProductCategory getById(int id) throws DAOException, PoolException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ProductCategory> getAll() throws DAOException, PoolException {
        Statement st = null;
        List<ProductCategory> categories = new ArrayList<>();

        try {
            connection = DBConnectionPool.getInstance().getConnection();
            st = connection.createStatement();
            ResultSet resultSet = st.executeQuery(GET_CATEGORIES_QUERY);
            while (resultSet.next()) {
                ProductCategory category = new ProductCategory();
                category.setId(resultSet.getInt("id"));
                category.setName(resultSet.getString("name"));
                int visibilityInt = resultSet.getInt("is_visible");
                boolean visible = true;
                if (visibilityInt == 0) {
                    visible = false;
                }
                category.setVisible(visible);
                categories.add(category);
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return categories;
    }

    @Override
    public ProductCategory findByName(String name) throws DAOException, PoolException {
        PreparedStatement st = null;
        ProductCategory category = new ProductCategory();

        try {
            connection = DBConnectionPool.getInstance().getConnection();
            st = connection.prepareStatement(FIND_BY_NAME_QUERY);
            st.setString(1, name);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()) {
                category.setId(resultSet.getInt("id"));
                category.setName(resultSet.getString("name"));
                int visibilityInt = resultSet.getInt("is_visible");
                boolean visible = true;
                if (visibilityInt == 0) {
                    visible = false;
                }
                category.setVisible(visible);
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("SQL query error!");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return category;
    }

    @Override
    public boolean add(ProductCategory t) throws DAOException, PoolException {
        ProductCategory category = (ProductCategory) t;
        boolean result = true;
        PreparedStatement ps = null;
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            ps = connection.prepareStatement(ADD_CATEGORY_QUERY);
            ps.setString(1, category.getName());
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }
        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return result;
    }

    @Override
    public boolean update(ProductCategory t) throws DAOException, PoolException {
        boolean result = true;
        PreparedStatement ps = null;
        try {
            connection = DBConnectionPool.getInstance().getConnection();
            ps = connection.prepareStatement(UPDATE_CATEGORY_QUERY);

            ps.setString(1, t.getName());
            int visible = 0;
            if (t.isVisible()) {
                visible = 1;
            }
            ps.setInt(2, visible);
            ps.setInt(3, t.getId());
            int count = 0;
            count = ps.executeUpdate();
            if (count == 0) {
                result = false;
            }

        } catch (SQLException e) {
            DAOException dao = new DAOException(e.getMessage());
            dao.setPropertyMessage("error.dao.sqlexception");
            logger.error(e.getMessage());
            throw dao;
        } finally {
            closeConnection();
        }
        return result;
    }

}
