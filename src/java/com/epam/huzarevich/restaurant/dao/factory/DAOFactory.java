/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.dao.factory;

import com.epam.huzarevich.restaurant.dao.mysql_impl.MySQLDAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IOrderDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductCategoryDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;

/**DAO FActory
 *
 * @author al94
 */
public abstract class DAOFactory {

    /**
     *
     * @return OrderDAOImpl
     */
    public abstract IOrderDAO getOrderDao();

    /**
     *
     * @return ProductDAOImpl
     */
    public abstract IProductDAO getProductDao();

    /**
     *
     * @return UserDAOImpl
     */
    public abstract IUserDAO getUserDao();

    /**
     *
     * @return ProductCategoryDAOImpl
     */
    public abstract IProductCategoryDAO getProductCategoryDao();

    /**
     *
     * @param dEnum
     * @return Mysql or any other DAOFactory
     */
    public static DAOFactory getDAOFactory(DAOEnum dEnum) {

        switch (dEnum) {
            case MYSQL:
                return new MySQLDAOFactory();
            case POSTGRESQL:
                return null;
            case XML:
                return null;
            default:
                return null;

        }

    }

}
