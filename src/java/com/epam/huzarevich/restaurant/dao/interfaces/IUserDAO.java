/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.dao.interfaces;

import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import java.util.List;

/**
 *
 * @author al94
 */
public interface IUserDAO {

    /** Find by Login
     *
     * @param parameter
     * @return User 
     * @throws DAOException
     * @throws PoolException
     */
    public User findByLogin(String parameter) throws DAOException, PoolException;
    
    /**Adds user
     *
     * @param user
     * @return boolean
     * @throws DAOException
     * @throws PoolException
     */
    public  boolean add(User user) throws DAOException, PoolException;
    
    /** Update user
     *
     * @param user
     * @return boolean
     * @throws DAOException
     * @throws PoolException
     */
    public  boolean update(User user) throws DAOException, PoolException;
    
    /**
     *
     * @return List of all Users
     * @throws DAOException
     * @throws PoolException
     */
    public  List<User> getAll() throws DAOException, PoolException;
    
    /** Find by Id
     *
     * @param id
     * @return User
     * @throws DAOException
     * @throws PoolException
     */
    public  User getById(int id) throws DAOException, PoolException;
}
