/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.dao.interfaces;

import com.epam.huzarevich.restaurant.entities.Product;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import java.util.List;

/**
 *
 * @author al94
 */
public interface IProductDAO {
    
    /**Find by Order id
     *
     * @param id
     * @return List of Product 
     * @throws DAOException
     * @throws PoolException
     */
    public List<Product> getProductsByOrderId(int id) throws DAOException, PoolException ;
    
    /**Adds product
     *
     * @param product
     * @return boolean
     * @throws DAOException
     * @throws PoolException
     */
    public  boolean add(Product product) throws DAOException, PoolException;
    
    /**Update product
     *
     * @param product
     * @return boolean
     * @throws DAOException
     * @throws PoolException
     */
    public  boolean update(Product product) throws DAOException, PoolException;
    
    /**
     *
     * @return List of all Product
     * @throws DAOException
     * @throws PoolException
     */
    public  List<Product> getAll() throws DAOException, PoolException;
    
    /**gets Product ById
     *
     * @param id
     * @return Product
     * @throws DAOException
     * @throws PoolException
     */
    public  Product getById(int id) throws DAOException, PoolException;
}
