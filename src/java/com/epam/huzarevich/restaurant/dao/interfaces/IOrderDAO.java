/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.dao.interfaces;

import com.epam.huzarevich.restaurant.entities.Order;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import java.util.List;

/**
 *
 * @author al94
 */
public interface IOrderDAO {

    /**
     *
     * @param userId
     * @return List of  Orders created by UserId
     * @throws DAOException
     * @throws PoolException
     */
    public List<Order> getAllOrdersByUserId(int userId) throws DAOException, PoolException;

    /**
     *
     * @return  last created Order
     * @throws DAOException
     * @throws PoolException
     */
    public Order getLastOrder() throws DAOException, PoolException;

    /** Add order
     *
     * @param order
     * @return last Order
     * @throws DAOException
     * @throws PoolException
     */
    public  boolean add(Order order) throws DAOException, PoolException;
    
    /**Updates Order
     *
     * @param order
     * @return boolean
     * @throws DAOException
     * @throws PoolException
     */
    public  boolean update(Order order) throws DAOException, PoolException;
    
    /**Updates Order
     *
     * @return boolean
     * @throws DAOException
     * @throws PoolException
     */
    public  List<Order> getAll() throws DAOException, PoolException;
    
    /**
     *
     * @param id
     * @return Order by Id
     * @throws DAOException
     * @throws PoolException
     */
    public  Order getById(int id) throws DAOException, PoolException;

}
