/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.dao.interfaces;

import com.epam.huzarevich.restaurant.entities.ProductCategory;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import java.util.List;

/**
 *
 * @author al94
 * 
 */
public interface IProductCategoryDAO {
    
    /**
     *
     * @param name
     * @return ProductCategory by its name
     * @throws DAOException
     * @throws PoolException
     */
    public ProductCategory findByName(String name) throws DAOException, PoolException ; 
   
    /**Adds ProductCategory
     *
     * @param category
     * @return boolean
     * @throws DAOException
     * @throws PoolException
     */
    public  boolean add(ProductCategory category) throws DAOException, PoolException;
   
    /**Updates ProductCategory
     *
     * @param category
     * @return boolean
     * @throws DAOException
     * @throws PoolException
     */
    public  boolean update(ProductCategory category) throws DAOException, PoolException;
   
    /**
     *
     * @return List of all Orders
     * @throws DAOException
     * @throws PoolException
     */
    public  List<ProductCategory> getAll() throws DAOException, PoolException;
   
    /**
     *
     * @param id
     * @return ProductCategory by id
     * @throws DAOException
     * @throws PoolException
     */
    public  ProductCategory getById(int id) throws DAOException, PoolException;
}
