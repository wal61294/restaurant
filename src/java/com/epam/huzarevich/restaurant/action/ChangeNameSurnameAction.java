/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.ChangeUserDataLogic;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**Action for changing name or surname
 *
 * @author al94
 */
public class ChangeNameSurnameAction extends Action {
    private final Logger logger = Logger.getLogger(this.getClass());
    @Override
    public String execute(SessionWrapper sessionWrapper) {
        User user = ChangeUserDataLogic.getUserWithChangedName(sessionWrapper);
        String password = sessionWrapper.getParameter("password");
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        String page = "/pages/success.jsp";
        IUserDAO dao = df.getUserDao();
        if (user.getName() != null && user.getSurname() != null) {
            try {
                if (user.getPassword().equals(password)) {
                    sessionWrapper.setSessionAttribute("userData", user);
                    sessionWrapper.setAttribute("successType", "changeName");
                    dao.update(user);
                    
                } else {
                    ChangeUserDataLogic.setDataForChangeName(sessionWrapper);
                    page = "/pages/change-name-surname.jsp";
                }
            } catch (DAOException e) {
                page = "/pages/error.jsp";
                sessionWrapper.setAttribute("error", e.getMessage());
                sessionWrapper.setAttribute("property", e.getPropertyMessage());
                logger.error(e.getMessage());
            } catch (PoolException e) {
                page = "/pages/error.jsp";
                sessionWrapper.setAttribute("error", e.getMessage());
                sessionWrapper.setAttribute("property", e.getPropertyMessage());
                logger.error(e.getMessage());
            }
            
        } else {
            page = "/pages/change-name-surname.jsp";
        }
        return page;
    }
    
}
