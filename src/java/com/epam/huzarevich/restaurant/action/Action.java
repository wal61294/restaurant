/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;

/**Abstract class of Action
 *
 * @author al94
 */
public abstract class Action {
  public abstract String execute(SessionWrapper sessionWrapper);   
}
