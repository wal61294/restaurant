
package com.epam.huzarevich.restaurant.action.factory;

import com.epam.huzarevich.restaurant.action.GoToStartPageAction;
import com.epam.huzarevich.restaurant.action.ChangePasswordAction;
import com.epam.huzarevich.restaurant.action.ChangeLocationAction;
import com.epam.huzarevich.restaurant.action.ChangeNameSurnameAction;
import com.epam.huzarevich.restaurant.action.ChangePhoneAction;
import com.epam.huzarevich.restaurant.action.GoToChangeLocationAction;
import com.epam.huzarevich.restaurant.action.GoToChangePhoneAction;
import com.epam.huzarevich.restaurant.action.ViewPersonalDataAction;
import com.epam.huzarevich.restaurant.action.GoToChangePasswordAction;
import com.epam.huzarevich.restaurant.action.GoToChangeNameSurnameAction;
import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.action.admin.AddToMenuAction;
import com.epam.huzarevich.restaurant.action.admin.ApproveAction;
import com.epam.huzarevich.restaurant.action.client.CancelOrderAction;
import com.epam.huzarevich.restaurant.action.admin.CancelOrderAdminAction;
import com.epam.huzarevich.restaurant.action.client.CreateOrderAction;
import com.epam.huzarevich.restaurant.action.admin.CreateProductAction;
import com.epam.huzarevich.restaurant.action.admin.DeleteFromMenuAction;
import com.epam.huzarevich.restaurant.action.admin.EditOrdersAction;
import com.epam.huzarevich.restaurant.action.admin.EditProductsAction;
import com.epam.huzarevich.restaurant.action.client.GoToOrderCreationAction;
import com.epam.huzarevich.restaurant.action.admin.GoToProductCreationAction;
import com.epam.huzarevich.restaurant.action.client.GoToRegisterAction;
import com.epam.huzarevich.restaurant.action.LoginAction;
import com.epam.huzarevich.restaurant.action.LogoutAction;
import com.epam.huzarevich.restaurant.action.client.ManageClientOrdersAction;
import com.epam.huzarevich.restaurant.action.admin.MarkDoneAction;
import com.epam.huzarevich.restaurant.action.client.RegisterAction;
import com.epam.huzarevich.restaurant.action.admin.SetCategoryInVisibleAction;
import com.epam.huzarevich.restaurant.action.admin.SetCategoryVisibleAction;
import com.epam.huzarevich.restaurant.action.admin.UpdateProductAction;
import com.epam.huzarevich.restaurant.action.ViewOrderAction;
import com.epam.huzarevich.restaurant.action.admin.AddCategoryAction;
import com.epam.huzarevich.restaurant.action.admin.AddCategoriesAction;
import com.epam.huzarevich.restaurant.action.admin.GoToUpdateProductAction;
import com.epam.huzarevich.restaurant.action.admin.ViewClientDataAction;

/**Enum for relation from action param from form to Action 
 *
 * @author al94
 */
public enum ActionEnum {

    /**
     *
     */
    LOGOUT {
        {
            this.action = new LogoutAction();
        }
    },

    /**
     *
     */
    ADDCATEGORY {
        {
            this.action = new AddCategoryAction();
        }
    },

    /**
     *
     */
    ADDCATEGORIES {
        {
            this.action = new AddCategoriesAction();
        }
    },

    /**
     *
     */
    GOTOPRODUCTCREATION {
        {
            this.action = new GoToProductCreationAction();
        }
    },

    /**
     *
     */
    CHANGEPASSWORD {
        {
            this.action = new ChangePasswordAction();
        }
    },

    /**
     *
     */
    CREATEPRODUCT {
        {
            this.action = new CreateProductAction();
        }
    },

    /**
     *
     */
    CHANGELOCATION {
        {
            this.action = new ChangeLocationAction();
        }
    },

    /**
     *
     */
    GOTOREGISTER {
        {
            this.action = new GoToRegisterAction();
        }
    },

    /**
     *
     */
    REGISTER {
        {
            this.action = new RegisterAction();
        }
    },

    /**
     *
     */
    GOTOORDERCREATION {
        {
            this.action = new GoToOrderCreationAction();
        }

    },

    /**
     *
     */
    CHANGEPHONE {
        {
            this.action = new ChangePhoneAction();
        }
    },

    /**
     *
     */
    DELETEFROMMENU {
        {
            this.action = new DeleteFromMenuAction();
        }

    },

    /**
     *
     */
    CHANGENAMESURNAME {
        {
            this.action = new ChangeNameSurnameAction();
        }

    },

    /**
     *
     */
    GOTOCHANGENAMESURNAME {
        {
            this.action = new GoToChangeNameSurnameAction();
        }
    },

    /**
     *
     */
    GOTOSTARTPAGE {
        {
            this.action = new GoToStartPageAction();
        }

    },

    /**
     *
     */
    GOTOCHANGEPASSWORD {
        {
            this.action = new GoToChangePasswordAction();
        }
    },

    /**
     *
     */
    GOTOCHANGEPHONE {
        {
            this.action = new GoToChangePhoneAction();
        }
    },

    /**
     *
     */
    GOTOCHANGELOCATION {
        {
            this.action = new GoToChangeLocationAction();
        }
    },

    /**
     *
     */
    SETCATEGORYVISIBLE {
        {
            this.action = new SetCategoryVisibleAction();
        }

    },

    /**
     *
     */
    SETCATEGORYINVISIBLE {
        {
            this.action = new SetCategoryInVisibleAction();
        }

    },

    /**
     *
     */
    ADDTOMENU {
        {
            this.action = new AddToMenuAction();
        }

    },

    /**
     *
     */
    MANAGECLIENTORDERS {
        {
            this.action = new ManageClientOrdersAction();
        }

    },

    /**
     *
     */
    EDITORDERS {
        {
            this.action = new EditOrdersAction();
        }
    },

    /**
     *
     */
    UPDATEPRODUCT {
        {
            this.action = new UpdateProductAction();
        }
    },

    /**
     *
     */
    EDITPRODUCTS {
        {
            this.action = new EditProductsAction();
        }
    },

    /**
     *
     */
    CREATEORDER {
        {
            this.action = new CreateOrderAction();
        }
    },

    /**
     *
     */
    VIEWORDER {
        {
            this.action = new ViewOrderAction();
        }
    },

    /**
     *
     */
    CANCELORDER {
        {
            this.action = new CancelOrderAction();
        }
    },

    /**
     *
     */
    CANCELORDERADMIN {

        {
            this.action = new CancelOrderAdminAction();
        }
    },

    /**
     *
     */
    APPROVEORDER {

        {
            this.action = new ApproveAction();
        }
    },

    /**
     *
     */
    MARKDONE {

        {
            this.action = new MarkDoneAction();
        }
    },

    /**
     *
     */
    VIEWPERSONALDATA {
        {
            this.action = new ViewPersonalDataAction();
        }
    },

    /**
     *
     */
    LOGIN {
        {
            this.action = new LoginAction();
        }
    },

    /**
     *
     */
    VIEWCLIENTDATA {
        {
            this.action = new ViewClientDataAction();
        }

    },

    /**
     *
     */
    GOTOPRODUCTUPDATE {
        {
            this.action = new GoToUpdateProductAction();
        }
    };

    Action action;

    /**Returns suitable for actionenum  Action
     *
     * @return
     */
    public Action getCurrentAction() {
        return action;
    }

}
