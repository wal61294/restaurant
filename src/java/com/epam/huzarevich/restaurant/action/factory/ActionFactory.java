/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.factory;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.action.EmptyAction;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**
 * Action Factory implements pattern command
 *
 * @author al94
 */
public class ActionFactory {

    private final Logger logger = Logger.getLogger(this.getClass());

    /**
     * find Action if it exists in ActionEnum or Empty Action if it is not
     * @param sessionWrapper
     * @return Action
     */
    public Action getAction(SessionWrapper sessionWrapper) {
        Action current = new EmptyAction();
        String action = sessionWrapper.getParameter("action");
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            ActionEnum currentEnum = ActionEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentAction();
        } catch (IllegalArgumentException e) {
            logger.error(e.getMessage());
        }
        return current;
    }

}
