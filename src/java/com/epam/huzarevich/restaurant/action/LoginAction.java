/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**Login action  for enter to the system 
 *
 * @author al94
 */
public class LoginAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        String login = sessionWrapper.getParameter("login");
        String pass = sessionWrapper.getParameter("password");
        String page = ConfigurationManager.getProperty("path.index");
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IUserDAO userdao = df.getUserDao();
        try {
            User user = userdao.findByLogin(login);
            if (user != null) {
                if (user.getPassword().equals(pass)) {
                    if (user.getRole().equals("admin")) {
                        sessionWrapper.setSessionAttribute("user", login);
                        sessionWrapper.setSessionAttribute("userData", user);
                        page = ConfigurationManager.getProperty("path.admin");
                    } else {
                        sessionWrapper.setSessionAttribute("user", login);
                        sessionWrapper.setSessionAttribute("userData", user);
                        sessionWrapper.setAttribute("name", user.getName());
                        page = ConfigurationManager.getProperty("path.client");
                    }

                } else {
                    sessionWrapper.setAttribute("errorLoginPassMessage",
                            "Incorrect Password");
                    page = ConfigurationManager.getProperty("path.index");
                }
            } else {
                sessionWrapper.setAttribute("errorLoginPassMessage",
                        "Incorrect Login");
                page = ConfigurationManager.getProperty("path.index");
            }
        } catch (DAOException e) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", e.getMessage());
            sessionWrapper.setAttribute("property", e.getPropertyMessage());
            logger.error(e.getMessage());
        } catch (PoolException e) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", e.getMessage());
            sessionWrapper.setAttribute("property", e.getPropertyMessage());
            logger.error(e.getMessage());
        }
        return page;
    }

}
