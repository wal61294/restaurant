/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.ChangeUserDataLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**changes adress of client
 *
 * @author al94
 */
public class ChangeLocationAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        String password = sessionWrapper.getParameter("password");
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        String page = ConfigurationManager.getProperty("path.success");
        IUserDAO dao = df.getUserDao();
        User user = ChangeUserDataLogic.getUserWithChangedLocation(sessionWrapper);
        if (user.getStreet() != null && user.getApartment() != null && user.getHouse() != null) {
            try {
                if (user.getPassword().equals(password)) {
                    sessionWrapper.setAttribute("successType", "changeLocation");
                    sessionWrapper.setSessionAttribute("userData", user);
                    dao.update(user);
                } else {
                    ChangeUserDataLogic.setDataForChangeLocation(sessionWrapper);
                    page = ConfigurationManager.getProperty("path.change-location");
                }
            } catch (DAOException e) {
                page = ConfigurationManager.getProperty("path.error");
                sessionWrapper.setAttribute("error", e.getMessage());
                sessionWrapper.setAttribute("property", e.getPropertyMessage());
                logger.error(e.getMessage());
            } catch (PoolException e) {
                page = ConfigurationManager.getProperty("path.error");
                sessionWrapper.setAttribute("error", e.getMessage());
                sessionWrapper.setAttribute("property", e.getPropertyMessage());
                logger.error(e.getMessage());
            }
        } else {
            page = ConfigurationManager.getProperty("path.change-location");
        }
        return page;
    }
}
