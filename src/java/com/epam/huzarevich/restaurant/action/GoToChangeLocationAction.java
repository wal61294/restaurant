/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.logic.ChangeUserDataLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;

/**Goes to start page change-location.jsp
 *
 * @author al94
 */
public class GoToChangeLocationAction extends Action {

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        ChangeUserDataLogic.setDataForChangeLocation(sessionWrapper);
        String page = ConfigurationManager.getProperty("path.change-location");
        return page;
    }

}
