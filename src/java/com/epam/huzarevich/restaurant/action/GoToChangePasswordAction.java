/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;

/**Goes to start page change-password.jsp
 *
 * @author al94
 */
public class GoToChangePasswordAction extends Action {

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        String page = ConfigurationManager.getProperty("path.change-password");
        return page;
    }

}
