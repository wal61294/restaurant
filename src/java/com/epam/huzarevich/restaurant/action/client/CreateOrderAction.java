/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.client;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IOrderDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductDAO;
import com.epam.huzarevich.restaurant.entities.Order;
import com.epam.huzarevich.restaurant.entities.Product;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import java.util.ArrayList;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;

/**Creates new Order
 *
 * @author al94
 */
public class CreateOrderAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        String page = ConfigurationManager.getProperty("path.success");
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IOrderDAO oDao = df.getOrderDao();
        IProductDAO pDao = df.getProductDao();
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        Calendar calendar = Calendar.getInstance();
        try {
            Order order = new Order();
            order.setUser(user);
            List<Product> products = pDao.getAll();
            List<Product> productsInOrder = new ArrayList<>();
            for (Product product : products) {
                if (product.isInMenu()) {
                    String param = String.valueOf(product.getId());
                    int productCol = Integer.parseInt(sessionWrapper.getParameter(param));
                    for (int i = 0; i < productCol; i++) {
                        productsInOrder.add(product);
                    }
                }
            }
            java.util.Date currentDate = calendar.getTime();
            Date date = new Date(currentDate.getTime());
            order.setOrderDate(date);
            Time time = new Time(currentDate.getTime());
            order.setOrderTime(time);
            order.setProducts(productsInOrder);
            if (oDao.add(order)) {
                sessionWrapper.setAttribute("successType", "createOrder");
            } else {
                page = ConfigurationManager.getProperty("path.error");
            }

        } catch (DAOException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.debug(ex.getMessage());
        } catch (PoolException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        }
        return page;
    }
}
