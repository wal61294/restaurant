/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.client;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IOrderDAO;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.CancelOrderLogic;
import com.epam.huzarevich.restaurant.logic.SetOrdersLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**Cancel orderby client
 *
 * @author al94
 */
public class CancelOrderAction extends Action {
    private final Logger logger = Logger.getLogger(this.getClass());
    @Override
    public String execute(SessionWrapper sessionWrapper) {
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IOrderDAO oDAO = df.getOrderDao();
        int id = Integer.parseInt(sessionWrapper.getParameter("orderId"));
        String page = ConfigurationManager.getProperty("path.view-client-orders");
        try {
            User user = (User) sessionWrapper.getSessionAttribute("userData");
            
            CancelOrderLogic.cancelOrder(oDAO.getById(id));
            SetOrdersLogic.setClientOrders(sessionWrapper, user.getId());

        } catch (DAOException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        } catch (PoolException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        }

        return page;
    }

}
