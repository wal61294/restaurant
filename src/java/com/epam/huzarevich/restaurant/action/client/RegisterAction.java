/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.client;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.RegisterLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**
 *
 * @author al94
 */
public class RegisterAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IUserDAO dao = df.getUserDao();
        String page = ConfigurationManager.getProperty("path.register");
        User user = RegisterLogic.getAndValidateUser(sessionWrapper);
        if (user.getPassword() != null) {
            String passwordRepeat = sessionWrapper.getParameter("repeatPassword");
            if (!passwordRepeat.equals(user.getPassword())) {
                RegisterLogic.setDataForReRegister(sessionWrapper, user);
                sessionWrapper.setAttribute("passwordRepeatError", "Passwords not match");
                page = ConfigurationManager.getProperty("path.register");
            } else {
                try {
                    if (RegisterLogic.checkIfUserExists(user)) {
                        RegisterLogic.setDataForReRegister(sessionWrapper, user);
                        page = ConfigurationManager.getProperty("path.register");;
                    } else {
                        dao.add(user);
                        sessionWrapper.setAttribute("successType", "register");
                        sessionWrapper.setAttribute("username", user.getName());
                        page = ConfigurationManager.getProperty("path.success");
                    }
                } catch (DAOException ex) {
                    page = ConfigurationManager.getProperty("path.error");
                    sessionWrapper.setAttribute("error", ex.getMessage());
                    sessionWrapper.setAttribute("property", ex.getPropertyMessage());
                    logger.error(ex.getMessage());
                } catch (PoolException ex) {
                    page = ConfigurationManager.getProperty("path.error");
                    sessionWrapper.setAttribute("error", ex.getMessage());
                    sessionWrapper.setAttribute("property", ex.getPropertyMessage());
                    logger.error(ex.getMessage());
                }
            }
        }
        return page;
    }

}
