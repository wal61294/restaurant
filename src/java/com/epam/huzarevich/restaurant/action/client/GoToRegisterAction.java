/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.client;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;

/**goes to register.jsp
 *
 * @author al94
 */
public class GoToRegisterAction extends Action {

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        return ConfigurationManager.getProperty("path.register");
    }

}
