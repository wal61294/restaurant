/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.client;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.SetOrdersLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**Set orders to view-client-orders.jsp
 *
 * @author al94
 */
public class ManageClientOrdersAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        String page = ConfigurationManager.getProperty("path.view-client-orders");
        try {
            User user = (User) sessionWrapper.getSessionAttribute("userData");
            SetOrdersLogic.setClientOrders(sessionWrapper, user.getId());
        } catch (DAOException e) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", e.getMessage());
            sessionWrapper.setAttribute("property", e.getPropertyMessage());
            logger.error(e.getMessage());
        } catch (PoolException e) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", e.getMessage());
            sessionWrapper.setAttribute("property", e.getPropertyMessage());
            logger.error(e.getMessage());
        }
        return page;
    }

}
