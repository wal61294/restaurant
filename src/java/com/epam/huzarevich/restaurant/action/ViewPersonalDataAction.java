/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;

/**
 *
 * @author al94
 */
public class ViewPersonalDataAction extends Action {

    /**
     *forwards to view-personal-data.jsp
     * @param sessionWrapper
     * @return
     */
    @Override
    public String execute(SessionWrapper sessionWrapper) {
        String page = ConfigurationManager.getProperty("path.view-personal-data");
        return page;
    }

}
