/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IOrderDAO;
import com.epam.huzarevich.restaurant.entities.Order;
import com.epam.huzarevich.restaurant.entities.Product;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.ViewOrderLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author al94
 */
public class ViewOrderAction extends Action {
    private final Logger logger = Logger.getLogger(this.getClass());

    /**displays order. Can be used by client and admin.
     *
     * @param sessionWrapper
     * @return
     */
    @Override
    public String execute(SessionWrapper sessionWrapper) {
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        int id = Integer.parseInt(sessionWrapper.getParameter("orderId"));
        IOrderDAO oDao = df.getOrderDao();
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        sessionWrapper.setAttribute("userType", user.getRole());
        String page = ConfigurationManager.getProperty("path.view-order");
        try {
            Order order = oDao.getById(id);
            List<Product> products = order.getProducts();
            Map<Product, Integer> productsWithTheirsNumbers
                    = ViewOrderLogic.getProductsWithTheirsNumbers(products);
            sessionWrapper.setAttribute("productsWithTheirsNumbers", productsWithTheirsNumbers);
            sessionWrapper.setAttribute("totalPrice", order.getPrice());
        } catch (DAOException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        } catch (PoolException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        }
        return page;
    }

}
