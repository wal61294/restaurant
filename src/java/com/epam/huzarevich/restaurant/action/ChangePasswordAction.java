/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.ValidateParameterLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**Changes password
 *
 * @author al94
 */
public class ChangePasswordAction extends Action {
    private final Logger logger = Logger.getLogger(this.getClass());
    @Override
    public String execute(SessionWrapper sessionWrapper) {
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        String password = sessionWrapper.getParameter("password");
        String nPassword = sessionWrapper.getParameter("newPassword");
        String passwordRepeat = sessionWrapper.getParameter("repeatNewPassword");
        String page = ConfigurationManager.getProperty("path.success");
        if (ValidateParameterLogic.checkPassword(nPassword)) {
            DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
            IUserDAO dao = df.getUserDao();
            try {
                if (user.getPassword().equals(password)) {
                    if (nPassword.equals(passwordRepeat)) {
                        sessionWrapper.setAttribute("passwordRepeatError", "password not Match");
                        user.setPassword(nPassword);
                        sessionWrapper.setAttribute("successType", "changePassword");
                        sessionWrapper.setSessionAttribute("userData", user);
                        dao.update(user);

                    } else {
                        page = ConfigurationManager.getProperty("path.change-password");
                    }

                }else{
                    page = ConfigurationManager.getProperty("path.change-password");
                    sessionWrapper.setAttribute("oldPasswordError", "Wrong Old Password");
                }
            } catch (DAOException e) {
                page = ConfigurationManager.getProperty("path.error");
                sessionWrapper.setAttribute("error", e.getMessage());
                sessionWrapper.setAttribute("property", e.getPropertyMessage());
                logger.error(e.getMessage());
            } catch (PoolException e) {
                page = ConfigurationManager.getProperty("path.error");
                sessionWrapper.setAttribute("error", e.getMessage());
                sessionWrapper.setAttribute("property", e.getPropertyMessage());
                logger.error(e.getMessage());
            }
        } else {
            page = ConfigurationManager.getProperty("path.change-password");
        }
        return page;
    }

}
