/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.admin;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductDAO;
import com.epam.huzarevich.restaurant.entities.Product;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.SetCategoriesLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**sets categories, and product data for product updation
 *
 * @author al94
 */
public class GoToUpdateProductAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        Integer id = Integer.parseInt(sessionWrapper.getParameter("productId"));
        String page = ConfigurationManager.getProperty("path.update-product");
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);

        IProductDAO pDao = df.getProductDao();
        try {

            Product product = pDao.getById(id);
            SetCategoriesLogic.setCategoriesForUpdateProduct(sessionWrapper, product.getCategory().getName());
            sessionWrapper.setAttribute("mass", product.getMass());
            sessionWrapper.setAttribute("price", product.getPrice());
            sessionWrapper.setAttribute("name", product.getName());
            sessionWrapper.setAttribute("category", product.getCategory().getName());
            sessionWrapper.setSessionAttribute("product", product);
        } catch (DAOException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        } catch (PoolException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        }
        return page;
    }

}
