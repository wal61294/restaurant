/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.admin;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;

/**
 *
 * @author al94
 */
public class ViewClientDataAction extends Action {

    private final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IUserDAO userdao = df.getUserDao();
        String login = sessionWrapper.getParameter("userLogin");
        String page = ConfigurationManager.getProperty("path.view-client-data");
        try {
            User user = userdao.findByLogin(login);
            sessionWrapper.setAttribute("name", user.getName());
            sessionWrapper.setAttribute("surname", user.getSurname());
            sessionWrapper.setAttribute("phone", user.getPhone());
            sessionWrapper.setAttribute("street", user.getStreet());
            sessionWrapper.setAttribute("house", user.getHouse());
            if (user.getApartment() != 0) {
                sessionWrapper.setAttribute("apartment", user.getApartment());
            }
            sessionWrapper.setAttribute("login", user.getLogin());
        } catch (DAOException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        } catch (PoolException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        }
        return page;

    }
}
