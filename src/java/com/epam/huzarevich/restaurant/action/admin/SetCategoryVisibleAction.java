/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.admin;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductCategoryDAO;
import com.epam.huzarevich.restaurant.entities.ProductCategory;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.SetProductsLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**Makes category invisible
 *
 * @author al94
 */
public class SetCategoryVisibleAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        String page = ConfigurationManager.getProperty("path.view-products-admin");
        String categoryName = sessionWrapper.getParameter("categoryName");
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IProductCategoryDAO pcDao = df.getProductCategoryDao();
        try {
            ProductCategory category = pcDao.findByName(categoryName);
            category.setVisible(true);
            pcDao.update(category);
            SetProductsLogic.setForAdmins(sessionWrapper);
        } catch (DAOException e) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", e.getMessage());
            sessionWrapper.setAttribute("property", e.getPropertyMessage());
            logger.error(e.getMessage());
        } catch (PoolException e) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", e.getMessage());
            sessionWrapper.setAttribute("property", e.getPropertyMessage());
            logger.error(e.getMessage());
        }
        return page;
    }

}
