/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.admin;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductCategoryDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductDAO;
import com.epam.huzarevich.restaurant.entities.Product;
import com.epam.huzarevich.restaurant.entities.ProductCategory;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.ValidateParameterLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**Creates product
 *
 * @author al94
 */
public class CreateProductAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        String page = ConfigurationManager.getProperty("path.error");
        Product product = new Product();
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IProductCategoryDAO pcDao = df.getProductCategoryDao();
        IProductDAO pDao = df.getProductDao();
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        String productName = sessionWrapper.getParameter("productName");
        if (ValidateParameterLogic.checkProductOrCategoryName(productName)) {
            try {
                product.setName(sessionWrapper.getParameter("productName"));
                product.setMass(Integer.parseInt(sessionWrapper.getParameter("productMass")));
                String categoryName = sessionWrapper.getParameter("category");

                ProductCategory productCategory = pcDao.findByName(categoryName);
                product.setCategory(productCategory);
                product.setPrice(Integer.parseInt(sessionWrapper.getParameter("productPrice")));

                if (pDao.add(product)) {
                    sessionWrapper.setAttribute("successType", "addProduct");
                    sessionWrapper.setAttribute("username", user.getName());
                    page = ConfigurationManager.getProperty("path.success");
                }

            } catch (DAOException ex) {
                page = ConfigurationManager.getProperty("path.error");
                sessionWrapper.setAttribute("error", ex.getMessage());
                sessionWrapper.setAttribute("property", ex.getPropertyMessage());
                logger.error(ex.getMessage());
            } catch (PoolException ex) {
                page = ConfigurationManager.getProperty("path.error");
                sessionWrapper.setAttribute("error", ex.getMessage());
                sessionWrapper.setAttribute("property", ex.getPropertyMessage());
                logger.error(ex.getMessage());
            }
        } else {
            sessionWrapper.setAttribute("productError", "Неправильный формат продукта");
            page = ConfigurationManager.getProperty("path.product-creation");
        }

        return page;
    }

}
