/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.admin;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductCategoryDAO;
import com.epam.huzarevich.restaurant.entities.ProductCategory;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import java.util.List;
import org.apache.log4j.Logger;

/**sets categories for product creation
 *
 * @author al94
 */
public class GoToProductCreationAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IProductCategoryDAO pcDAO = df.getProductCategoryDao();
        String page = ConfigurationManager.getProperty("path.product-creation");
        try {
            List<ProductCategory> categories = pcDAO.getAll();
            sessionWrapper.setAttribute("productCategories", categories);
        } catch (DAOException e) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", e.getMessage());
            sessionWrapper.setAttribute("property", e.getPropertyMessage());
            logger.error(e.getMessage());
        } catch (PoolException e) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", e.getMessage());
            sessionWrapper.setAttribute("property", e.getPropertyMessage());
            logger.error(e.getMessage());
        }
        return page;
    }

}
