/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.admin;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductCategoryDAO;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductDAO;
import com.epam.huzarevich.restaurant.entities.Product;
import com.epam.huzarevich.restaurant.entities.ProductCategory;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**Updates product
 *
 * @author al94
 */
public class UpdateProductAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IProductCategoryDAO pcDao = df.getProductCategoryDao();
        IProductDAO pDao = df.getProductDao();

        String page = ConfigurationManager.getProperty("path.product-update-success");
        Product product = (Product) sessionWrapper.getSessionAttribute("product");
        product.setName(sessionWrapper.getParameter("productName"));
        product.setMass(Integer.parseInt(sessionWrapper.getParameter("productMass")));
        product.setPrice(Integer.parseInt(sessionWrapper.getParameter("productPrice")));
        try {
            ProductCategory productCategory = pcDao.findByName(sessionWrapper.getParameter("category"));
            product.setCategory(productCategory);
            pDao.update(product);
        } catch (DAOException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        } catch (PoolException ex) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", ex.getMessage());
            sessionWrapper.setAttribute("property", ex.getPropertyMessage());
            logger.error(ex.getMessage());
        }
        return page;

    }

}
