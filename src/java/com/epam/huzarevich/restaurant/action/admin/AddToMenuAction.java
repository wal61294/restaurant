/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.admin;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductDAO;
import com.epam.huzarevich.restaurant.entities.Product;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.SetProductsLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**adds product to menu
 *
 * @author al94
 */
public class AddToMenuAction extends Action {
    private final Logger logger = Logger.getLogger(this.getClass());
    @Override
    public String execute(SessionWrapper sessionWrapper) {
        String page = ConfigurationManager.getProperty("path.view-products-admin");
        Integer id = Integer.parseInt(sessionWrapper.getParameter("productId"));
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IProductDAO pdao = df.getProductDao();
        try {
            Product product = pdao.getById(id);
            product.setIsInMenu(true);
            pdao.update(product);
            SetProductsLogic.setForAdmins(sessionWrapper);
        } catch (DAOException e) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", e.getMessage());
            sessionWrapper.setAttribute("property", e.getPropertyMessage());
            logger.error(e.getMessage());
        } catch (PoolException e) {
            page = ConfigurationManager.getProperty("path.error");
            sessionWrapper.setAttribute("error", e.getMessage());
            sessionWrapper.setAttribute("property", e.getPropertyMessage());
            logger.error(e.getMessage());
        }
        return page;
    }

}
