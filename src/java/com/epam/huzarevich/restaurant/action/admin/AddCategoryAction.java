/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action.admin;

import com.epam.huzarevich.restaurant.action.Action;
import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductCategoryDAO;

import com.epam.huzarevich.restaurant.entities.ProductCategory;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.ValidateParameterLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**adds category
 *
 * @author al94
 */
public class AddCategoryAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        IProductCategoryDAO pcDao = df.getProductCategoryDao();
        String page = ConfigurationManager.getProperty("path.error");
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        String categoryName = sessionWrapper.getParameter("category");
        if (ValidateParameterLogic.checkProductOrCategoryName(categoryName)) {

            ProductCategory category = new ProductCategory();
            category.setName(categoryName);
            try {
                if (pcDao.add(category)) {
                    page = ConfigurationManager.getProperty("path.success");
                    sessionWrapper.setAttribute("username", user.getName());
                    sessionWrapper.setAttribute("successType", "addCategory");
                }
            } catch (DAOException ex) {
                page = ConfigurationManager.getProperty("path.error");
                sessionWrapper.setAttribute("error", ex.getMessage());
                sessionWrapper.setAttribute("property", ex.getPropertyMessage());
                logger.error(ex.getMessage());
            } catch (PoolException ex) {
                page = ConfigurationManager.getProperty("path.error");
                sessionWrapper.setAttribute("error", ex.getMessage());
                sessionWrapper.setAttribute("property", ex.getPropertyMessage());
                logger.error(ex.getMessage());
            }
        } else {
            sessionWrapper.setAttribute("categoryError", "Wrong format for category");
            page = ConfigurationManager.getProperty("path.add-category");

        }
        return page;
    }

}
