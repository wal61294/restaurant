/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;

/**
 *
 * @author al94
 */
public class GoToStartPageAction extends Action {
  
    /**Goes to start page
     *
     * @param sessionWrapper
     * @return page in order of type of user
     */
    @Override
    public String execute(SessionWrapper sessionWrapper) {
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        sessionWrapper.setSessionAttribute("userData", user);
        String page = "";
        if (user.getRole().equals("client")) {
            page = ConfigurationManager.getProperty("path.client");
        } else {
            page = ConfigurationManager.getProperty("path.admin");
        }
        return page;
    }

}
