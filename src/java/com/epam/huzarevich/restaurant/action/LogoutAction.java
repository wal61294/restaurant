/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;

/**Removes session
 *
 * @author al94
 */
public class LogoutAction extends Action{

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        sessionWrapper.removeSession();
        return ConfigurationManager.getProperty("path.index");
    }
    
}
