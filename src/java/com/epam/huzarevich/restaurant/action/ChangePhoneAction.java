/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.action;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.entities.User;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import com.epam.huzarevich.restaurant.logic.ChangeUserDataLogic;
import com.epam.huzarevich.restaurant.logic.ValidateParameterLogic;
import com.epam.huzarevich.restaurant.resourse.ConfigurationManager;
import com.epam.huzarevich.restaurant.wrapper.SessionWrapper;
import org.apache.log4j.Logger;

/**Changes phonenumber
 * 
 *
 * @author al94
 */
public class ChangePhoneAction extends Action {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public String execute(SessionWrapper sessionWrapper) {
        User user = (User) sessionWrapper.getSessionAttribute("userData");
        String phoneString = sessionWrapper.getParameter("phone");
        String password = sessionWrapper.getParameter("password");
        DAOFactory df = DAOFactory.getDAOFactory(DAOEnum.MYSQL);
        String page = ConfigurationManager.getProperty("path.success");
        IUserDAO dao = df.getUserDao();
        if (ValidateParameterLogic.checkPhone(phoneString)) {
            int phone = Integer.parseInt(phoneString);
            try {
                if (user.getPassword().equals(password)) {
                    user.setPhone(phone);
                    dao.update(user);
                    sessionWrapper.setSessionAttribute("userData", user);
                    sessionWrapper.setAttribute("successType", "changePhone");
                } else {
                    ChangeUserDataLogic.setDataForChangePhone(sessionWrapper);
                    page = ConfigurationManager.getProperty("path.change-phone");

                }
            } catch (DAOException e) {
                page = ConfigurationManager.getProperty("path.error");
                sessionWrapper.setAttribute("error", e.getMessage());
                sessionWrapper.setAttribute("property", e.getPropertyMessage());
                logger.error(e.getMessage());
            } catch (PoolException e) {
                page = ConfigurationManager.getProperty("path.error");
                sessionWrapper.setAttribute("error", e.getMessage());
                sessionWrapper.setAttribute("property", e.getPropertyMessage());
                logger.error(e.getMessage());
            }

        } else {
            ChangeUserDataLogic.setDataForChangePhone(sessionWrapper);
            sessionWrapper.setAttribute("phoneError", "Phone format error");
            page = ConfigurationManager.getProperty("path.change-phone");

        }
        return page;
    }

}
