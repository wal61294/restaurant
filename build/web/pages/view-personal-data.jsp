<%-- 
    Document   : view-personal-data
    Created on : 10.04.2016, 11:50:14
    Author     : al94
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="changeuserdata"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <style>
        .divUserType{
            display:none;
        }
    </style>
    <body>
        <h2><fmt:message key="changeuserdata"/></h2>
        <div class="divUserType">${type}</div>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToChangeNameSurname"/>
                <input class="btn-lg btn-success changesmth firstchs" type="submit" value="<fmt:message key="changens"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToChangePassword"/>
                <input class="btn-lg btn-primary changesmth" type="submit" value="<fmt:message key="changeps"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToChangeLocation"/>
                <input class="btn-lg btn-info changesmth" type="submit" value="<fmt:message key="changeloc"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToChangePhone"/>
                <input class="btn-lg btn-warning changesmth" type="submit" value="<fmt:message key="changephone"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input type="submit"class="logout btn-danger" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToStartPage"/>
                <input type="submit" class="btn-link " value="<fmt:message key="back"/>" >
            </p>
        </form>
        <footer id="static-footer"><ct:custom/></footer>
    </body>
</html>
