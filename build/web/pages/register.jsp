<%-- 
    Document   : register
    Created on : 08.03.2016, 16:05:25
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src='js/jquery-2.2.1.min.js'></script>
        <script type="text/javascript" src='js/register.js'></script>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
        <title><fmt:message key="register"/></title>
    </head>
    <body>
        <h2><fmt:message key="registernu"/></h2>
        <form id="register-form" method="post" action="controller">
            <p><input type="hidden" name="action" value="register"/> 
            <p><label for="name" ><fmt:message key="name"/>:</label><input style="text-transform: capitalize;" id="name" required pattern="[-A-ZА-Яa-zа-я]{1,30}" name="name" onchange="inputNameAction(this)" class="${ok} form-control" type="text" maxlength="35" size="35"  value="${name}"/><span class="bg-danger" id="nameSp">${nameError}</span>
            <p><label for="surnameInp" ><fmt:message key="surname"/>:</label><input style="text-transform: capitalize;" required pattern="[-A-ZА-Яa-zа-я]{1,30}"   onchange="inputSurnameAction(this)" class="${ok} form-control" name="surname" type="text" maxlength="35" size="35" id="surnameInp" value="${surname}"/><span class="bg-danger" id="surname">${surnameError}</span>
            <p><label for="login" >Login:</label><input pattern="^[a-zA-Z][a-zA-Z0-9_\.]{2,20}$" required maxlength="10"  name="login" onchange="inputLoginAction(this)" class="${ok} form-control" type="login" size="35" id="login" value="${login}"/><span class="bg-danger" id="loginSp">${changeLogin}</span>
            <p><label for="street" ><fmt:message key="street"/>:</label><input style="text-transform: capitalize;" required pattern="^[А-Яа-я _]*[А-Яа-я][А-Яа-я _]*$" type="login" size="35" maxlength="30" class="${ok} form-control" onchange="inputStreetAction(this)" id="street" value="${street}" name="street"/><span class="bg-danger" id="stSp">${streetError}</span>
            <p><label for="house" ><fmt:message key="house"/>:</label><input class="${ok} form-control"  required pattern="^[0-9][0-9/А-Яа-я]+"  type="login" size="5" maxlength="5" id="house" value="${house}" onchange="inputHouseAction(this)" name="house"/><span class="bg-danger" id="houseSp">${houseError}</span>
            <p><label for="apartment" ><fmt:message key="apartment"/>:</label><input class="${ok} form-control"  pattern="^[0-9]+$" type="login" size="5" id="apartment" value="${apartment}" onchange="inputApartmentAction(this)" name="apartment"/><span class="bg-danger" id="apSp">${apartmentError}</span>
            <p><label for="tel" ><fmt:message key="phone"/>:</label><span id="telIndex">+375</span><input class="${ok} form-control" required pattern="[0-9]{9}" id="tel" minlength="9" size="9" maxlength="9" onchange="inputTelAction(this)" value="${phone}" name="phone"/><span class="bg-danger" id="telSp">${phoneError}</span>
            <p><label for="password" ><fmt:message key="password"/>:</label><input class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}" required id="password"  onchange="inputPasswordAction(this)" class="${ok}" name="password"  type="password" size="35"  value="${password}"/><span class="bg-danger" id="status">${passwordError}</span>
            <p><label for="passwordRepeat" ><fmt:message key="password.repeat"/>:</label><input class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}"  required id="passwordRepeat"  onchange="repeatPasswordAction(this)" name="repeatPassword" type="password" size="35" /> <span class="bg-danger" id="statusRepeat">${passwordRepeatError}</span>
            <p><input id="button" class="btn btn-primary" type="submit" value="<fmt:message key="button.gotoregister"/>"/>
        </form>
        <footer id="static-footer"><ct:custom/></footer>
    </body>
</html>
