<%-- 
    Document   : add-category
    Created on : 31.03.2016, 20:42:45
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="addcategory"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h2><fmt:message key="addcategory"/></h2>
        <form id="add-category-form"  action="controller" method="post">
            <input type="hidden" name="action" value="addCategory"/>
            <p> <input class="form-control"  required name="category" type="login" size="35" placeholder="<fmt:message key="input.category"/>" pattern="[-&quot«A-ZА-Яa-zа-я\\s»]{1,50}"/></p><span>${categoryError}</span>
            <p><input id="submit-category" class="btn btn-primary btadcat" type="submit"                               
                       value="<fmt:message key="addCategory"/>"/><p/> 
        </form> 
        <form method="post" action="controller">
            <input type="hidden" name="action" value="editProducts">
            <input class="btn-link" type="submit" value="<fmt:message key="back"/>">
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <footer><ct:custom/></footer>
    </body>
</html>
