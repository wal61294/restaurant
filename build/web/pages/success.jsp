<%-- 
    Document   : register-success
    Created on : 13.03.2016, 10:23:37
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="success"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
        <style>
            .succcessType{
                display: none;
            }
        </style>
    </head>
    <body>
        <div class="succcessType">  ${successType}</div>    

        <c:choose>
            <c:when test="${successType=='register'}">
                <h1> ${username} ,<fmt:message key="success.register"/> !</h1>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="logout"/>
                        <input type="submit" value="<fmt:message key="logout"/>" class="btn-danger logout" >
                    </p>
                </form>
            </c:when>

            <c:when test="${successType=='changePassword'}">
                <h1> ${username}, <fmt:message key="success.changepassword"/> </h1>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="logout"/>
                        <input type="submit" class="btn-danger logout" value="<fmt:message key="logout"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="goToStartPage"/>
                        <input type="submit" value="<fmt:message key="gotostart"/>" class="goToStartPage btn-link" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="viewPersonalData"/>
                        <input type="submit" class="btn-link" value="<fmt:message key="back"/>" >
                    </p>
                </form>
            </c:when>
            <c:when test="${successType=='changePhone'}">
                <h1> ${username}, <fmt:message key="success.changephone"/> </h1>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="logout"/>
                        <input type="submit" class="btn-danger logout" value="<fmt:message key="logout"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="goToStartPage"/>
                        <input type="submit" value="<fmt:message key="gotostart"/>" class="goToStartPage btn-link">
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="viewPersonalData"/>
                        <input type="submit"class="btn-link" value="<fmt:message key="back"/>" >
                    </p>
                </form>
            </c:when>
            <c:when test="${successType=='changeLocation'}">
                <h1>Dear ${username}, <fmt:message key="success.changelocation"/> </h1>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="logout"/>
                        <input type="submit" class="btn-danger logout" value="<fmt:message key="logout"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="goToStartPage"/>
                        <input type="submit" value="<fmt:message key="gotostart"/>" class="goToStartPage btn-link" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="viewPersonalData"/>
                        <input type="submit" class="btn-link" value="<fmt:message key="back"/>" >
                    </p>
                </form>
            </c:when>
            <c:when test="${successType=='changeName'}">
                <h1>Dear ${username} <fmt:message key="success.changename"/> </h1>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="logout"/>
                        <input type="submit" class="btn-danger logout" value="<fmt:message key="logout"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="goToStartPage"/>
                        <input type="submit" class="goToStartPage btn-link" value="<fmt:message key="gotostart"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="viewPersonalData"/>
                        <input type="submit" class="btn-link" value="<fmt:message key="back"/>" >
                    </p>
                </form>
            </c:when>
            <c:when test="${successType=='addProduct'}">
                <h1>Dear ${username} you've been succesfully added a new product </h1>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="logout"/>
                        <input type="submit" class="btn-danger logout" value="<fmt:message key="logout"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="goToStartPage"/>
                        <input type="submit" class="goToStartPage btn-link" value="<fmt:message key="gotostart"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="center">
                        <input type="hidden" name="action" value="editProducts"/>
                        <input type="submit" class="btn-link"  value="back" />		
                    </p>
                </form>
            </c:when>
            <c:when test="${successType=='createOrder'}">
                <h1>Dear ${username} you've been succesfully created a new order </h1>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="logout"/>
                        <input type="submit" class="btn-danger logout" value="<fmt:message key="logout"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="goToStartPage"/>
                        <input type="submit" class="goToStartPage btn-link" value="<fmt:message key="gotostart"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="manageClientOrders"/>
                        <input type="submit" class=" btn-link"  value="<fmt:message key="gotostart"/>" >
                    </p>
                </form>
            </c:when>
            <c:when test="${successType=='addCategory'}">
                <h1>Dear ${username} you've been succesfully added a new product category </h1>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden"  name="action" value="logout"/>
                        <input type="submit" class="btn-danger logout" value="<fmt:message key="logout"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="right">
                        <input type="hidden" name="action" value="goToStartPage"/>
                        <input type="submit" class="goToStartPage btn-link" value="<fmt:message key="gotostart"/>" >
                    </p>
                </form>
                <form  method="POST" action="controller">
                    <p align="center">
                        <input type="hidden" name="action" value="editProducts"/>
                        <input type="submit" class="btn-link" value="<fmt:message key="back"/>" />		
                    </p>
                </form>
            </c:when>



        </c:choose>
        <footer id="static-footer" class="product-footer"><ct:custom/></footer>
    </body>
</html>
