<%-- 
    Document   : view-orders
    Created on : 05.03.2016, 18:28:12
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="ordrmanagin"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src='js/jquery-2.2.1.min.js'></script>
        <script type="text/javascript" src='js/client-orders.js'></script>
    </head>

    <body>
        <h2><fmt:message key="ordrmanagin"/></h2>
        <span onclick="showNew()" class="order-spans"> <fmt:message key="neworders"/> <span class="hsdesc"><fmt:message key="shhi"/></span></span>
        <table class="table" id="newOrdersTable">
            <thead>
                <tr>
                    <td>Order Price</td>
                    <th>Order Date</th>
                    <th>Order Time</th>
                    <th>Username</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <c:forEach var="order" items="${newOrders}">
                <tr>

                    <td>${order.price}</td>
                    <td>${order.orderDate}</td>
                    <td>${order.orderTime}</td>
                    <td><form method="post" action="controller">
                            <input type="hidden" name="action" value="viewClientData">
                            <input type="hidden" name="userLogin" value="${order.user.login}">
                            <input class="btn btn-link link-usr"type="submit" value="${order.user.login}">
                        </form></td>

                    <td> <form method="post" action="controller">
                            <input type="hidden" name="action" value="viewOrder">
                            <input type="hidden" name="orderId" value="${order.id}">
                            <input class="btn btn-info"type="submit" value="<fmt:message key="vieworder"/>">
                        </form></td>
                    <td> <form method="post" action="controller">
                            <input type="hidden" name="action" value="cancelOrderAdmin">
                            <input type="hidden" name="orderId" value="${order.id}">
                            <input class="btn btn-danger" type="submit" value="<fmt:message key="cancelorder"/>">
                        </form></td>
                    <td> <form method="post" action="controller">
                            <input type="hidden" name="action" value="approveOrder">
                            <input type="hidden" name="orderId" value="${order.id}">
                            <input class="btn btn-success" type="submit" value="<fmt:message key="apprordr"/>">
                        </form></td>
                </tr>
            </c:forEach>
        </table>
        <span onclick="showInTransit()" class="order-spans"> <fmt:message key="intransit"/> <span class="hsdesc"><fmt:message key="shhi"/></span></span>
        <table class="table" id="undone-user-orders">
            <thead>
                <tr>
                    <th><fmt:message key="price"/></th>
                    <th><fmt:message key="date"/></th>
                    <th><fmt:message key="time"/></th>
                    <th>login</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <c:forEach var="order" items="${undoneOrders}">
                <tr>

                    <td>${order.price}</td>
                    <td>${order.orderDate}</td>
                    <td>${order.orderTime}</td>
                    <td><form method="post" action="controller">
                            <input type="hidden" name="action" value="viewClientData">
                            <input type="hidden" name="userLogin" value="${order.user.login}">
                            <input class="btn btn-link link-usr"type="submit" value="${order.user.login}">
                        </form></td>
                    <td> <form method="post" action="controller">
                            <input type="hidden" name="action" value="viewOrder">
                            <input type="hidden" name="orderId" value="${order.id}">
                            <input class="btn btn-info" type="submit" value="<fmt:message key="vieworder"/>">
                        </form></td>
                    <td> <form method="post" action="controller">
                            <input type="hidden" name="action" value="cancelOrderAdmin">
                            <input type="hidden" name="orderId" value="${order.id}">
                            <input class="btn btn-danger" type="submit" value="<fmt:message key="cancelorder"/>">
                        </form></td>

                    <td> <form method="post" action="controller">
                            <input type="hidden" name="action" value="markDone">
                            <input type="hidden" name="orderId" value="${order.id}">
                            <input class="btn btn-success" type="submit" value="<fmt:message key="markdone"/>">
                        </form></td>
                </tr>

            </c:forEach>
        </table>
        <span onclick="showCancelled()" class="order-spans"> Отклоненные<span class="hsdesc"><fmt:message key="shhi"/></span></span>
        <table class="table" id="cancelled-orders">
            <thead>
                <tr>

                    <td><fmt:message key="price"/></td>
                    <th><fmt:message key="date"/></th>
                    <th><fmt:message key="time"/></th>
                    <th>login</th>
                    <th></th>

                </tr>
            </thead>
            <c:forEach var="order" items="${disapprovedOrders}">
                <tr>

                    <td>${order.price}</td>
                    <td>${order.orderDate}</td>
                    <td>${order.orderTime}</td>
                    <td><form method="post" action="controller">
                            <input type="hidden" name="action" value="viewClientData">
                            <input type="hidden" name="userLogin" value="${order.user.login}">
                            <input class="btn btn-link link-usr"type="submit" value="${order.user.login}">
                        </form></td>
                    <td> <form method="post" action="controller">
                            <input type="hidden" name="action" value="viewOrder">
                            <input type="hidden" name="orderId" value="${order.id}">
                            <input class="btn btn-info" type="submit" value="<fmt:message key="vieworder"/>">
                        </form></td>
                </tr>
            </c:forEach>
        </table>
        <span onclick="showCancelledByYou()" class="order-spans"> <fmt:message key="cancelledbyu"/><span class="hsdesc"><fmt:message key="shhi"/></span></span>
        <table class="table" id="cancelled-by-you">
            <thead>
                <tr>
                    <td><fmt:message key="price"/></td>
                    <th><fmt:message key="date"/></th>
                    <th><fmt:message key="time"/></th>
                    <th>login</th>
                    <th></th>
                </tr>
            </thead>

            <c:forEach var="order" items="${cancelledOrders}">
                <tr>
                    <td>${order.price}</td>
                    <td>${order.orderDate}</td>
                    <td>${order.orderTime}</td>
                    <td><form method="post" action="controller">
                            <input type="hidden" name="action" value="viewClientData">
                            <input type="hidden" name="userLogin" value="${order.user.login}">
                            <input class="btn btn-link link-usr"type="submit" value="${order.user.login}">
                        </form></td>
                    <td> <form method="post" action="controller">
                            <input type="hidden" name="action" value="viewOrder">
                            <input type="hidden" name="orderId" value="${order.id}">
                            <input class="btn btn-info" type="submit" value="<fmt:message key="vieworder"/>">
                        </form></td>
                </tr>
            </c:forEach>
        </table>
        <span onclick="showDoneOrders()" class="order-spans"> <fmt:message key="done"/><span class="hsdesc"><fmt:message key="shhi"/></span></span>
        <table class="table" id="done-orders">
            <thead>
            <th><fmt:message key="donedate"/></th>
            <th><fmt:message key="donetime"/></th>
            <th><fmt:message key="price"/></th>
            <th><fmt:message key="date"/></th>
            <th><fmt:message key="time"/></th>
            <th>login</th>
            <th></th>
        </thead>
        <c:forEach var="order" items="${doneOrders}">
            <tr>
                <td>${order.doneOrderDate}</td>
                <td>${order.doneOrderTime}</td>
                <td>${order.price}</td>
                <td>${order.orderDate}</td>
                <td>${order.orderTime}</td>
                <td><form method="post" action="controller">
                        <input type="hidden" name="action" value="viewClientData">
                        <input type="hidden" name="userLogin" value="${order.user.login}">
                        <input class="btn btn-link link-usr"type="submit" value="${order.user.login}">
                    </form></td>
                <td> <form method="post" action="controller">
                        <input type="hidden" name="action" value="viewOrder">
                        <input type="hidden" name="orderId" value="${order.id}">
                        <input class="btn btn-info" type="submit" value="<fmt:message key="vieworder"/>">
                    </form></td>
            </tr>
        </c:forEach>
    </table>
    <form  method="POST" action="controller">
        <p align="right">
            <input type="hidden" name="action" value="logout"/>
            <input  class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
        </p>
    </form>
    <form  method="POST" action="controller">
        <p align="right">
            <input type="hidden" name="action" value="goToStartPage"/>
            <input type="submit" class="btn-link" value="<fmt:message key="back"/>" >
        </p>
    </form>

</body>
</html>
