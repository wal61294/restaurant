<%-- 
    Document   : create-order
    Created on : 13.03.2016, 22:30:24
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src='js/jquery-2.2.1.min.js'></script>
        <title><fmt:message key="createorder"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src='js/create-order.js'></script>
        
        
    </head>
    <body>
        <form id="create-order-form" method="POST" action="controller">
            <ul >
                <c:forEach var="category" items="${categoryProducts}">
                    <li  class="category_li_order">
                        <span onclick="showUL(this)" class="name">${category.name} <span class="hsdesc">[раскрыть/закрыть]</span></span>
                        <ul class="list-group slidelist">
                            <li class=" list-group-item about_order_li">
                                <div class="name"><fmt:message key="food.name"/></div>
                                <div class="price"><fmt:message key="price"/></div>
                                <div class="mass"><fmt:message key="mass"/></div>
                                <span class="number"><fmt:message key="amount"/></span>
                            </li>
                            <c:forEach var="product" items="${category.products}">
                                <li class="productLI list-group-item">
                                    <div class="name">${product.name}</div>
                                    <div class="price">${product.price}</div>
                                    <div class="mass">${product.mass}</div>
                                    <input onchange="calculatePrice()" min="0" max="10" class="form-control numborder" value="0" type="number" name="${product.id}" />
                                </li>
                            </c:forEach>
                        </ul>
                    </li>

                </c:forEach>
            </ul>

            <span id="outerTotalSpan"><fmt:message key="total"/>:  <span id="total"> </span><fmt:message key="rubles"/> </span>

            <p align="right">
                <input type="hidden" name="action" value="createOrder"/>
                <input class="btn btn-primary changeprim" id="buttonCreateOrder" type="submit" value="<fmt:message key="createorder"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input type="submit" class="btn-danger logout" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToStartPage"/>
                <input type="submit" class="btn-link" value="<fmt:message key="back"/>" >
            </p>
        </form>
        <footer id="static-footer"><ct:custom/></footer>
    </body>
</html>
