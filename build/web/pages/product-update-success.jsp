<%-- 
    Document   : product-update-success
    Created on : 02.04.2016, 16:41:54
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Success product update</title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h2>Продукт успешно обновлен</h2>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToStartPage"/>
                <input class="goToStartPage btn-link" type="submit" value="<fmt:message key="gotostart"/>" >
            </p>
        </form>
        <footer><ct:custom/></footer>
    </body>
</html>
