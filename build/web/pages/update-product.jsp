<%-- 
    Document   : updateProduct
    Created on : 01.04.2016, 23:54:18
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update product</title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h2>Update product</h2>
        <form method="post" id="productUpdateForm" action="controller">
            <p><input class="form-control"  type="hidden" name="action" value="updateProduct"/> 
            <p><input class="form-control" pattern="[-&quot«A-ZА-Яa-zа-я\s»]{1,50}"   name="productName" value="${name}" type="text" size="35" placeholder="Введите название продукта" /> 

            <p><span>Категория продукта:</span>    <select  class="form-control"  name="category" id="category" form="productUpdateForm">
                    <option selected  value="${category}" selected >${category}</option>
                    <c:forEach var="category" items="${productCategories}">
                        <option value="${category.name}">${category.name}</option>
                    </c:forEach>
                </select>

            <p><input class="form-control" value="${mass}" required min="1" name="productMass" type="number" size="35" placeholder="Введите массу продукта" /> 
            <p><input class="form-control" value="${price}" required min="1" name="productPrice" type="number" size="35" placeholder="Введите цену продукта" /> 
            <p><input class="btn btn-primary changeprim" id="button" type="submit" value="update product"/>
        </form>
        <form method="post" action="controller">
            <input type="hidden" name="action" value="editProducts">
            <input type="submit" value="<fmt:message key="back"/>" class="btn-link">
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <footer><ct:custom/></footer>
    </body>
</html>
