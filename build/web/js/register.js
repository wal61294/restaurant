window.onload = function () {
    $('#button').prop("disabled", true);
};

var password = '';
var passwordRepeat = '';
var snReg = /^\s*[A-ZА-Я][a-zа-я]+('[a-zа-я]+|-[A-ZА-Я][a-zа-я]+)?\s*$/;
function inputHouseAction(house) {
    var houseVal = house.value;
    var reg = "^[1-9][0-9А-Яа-я/]";
    if (houseVal.match(reg)) {
        $("#houseSp").text("");
        $(house).addClass("ok");
    } else {
        $("#houseSp").text("Wrong format");
        $(house).removeClass("ok");
    }
    if ($(".ok").size() === 8) {
        $('#button').prop("disabled", false);
    } else {
        $('#button').prop("disabled", true);
    }
}
function inputStreetAction(street) {
    var stVal = street.value;
    var reg = /^[А-Яа-я _]*[А-Яа-я][А-Яа-я _]*$/;
    if (stVal.match(reg)) {
        $("#stSp").text("");
        $(street).addClass("ok");
    } else {
        $("#stSp").text("Wrong format");
        $(street).removeClass("ok");
    }
    if ($(".ok").size() === 8) {
        $('#button').prop("disabled", false);
    } else {
        $('#button').prop("disabled", true);
    }
}
function inputApartmentAction(apartment) {
    var apVal = apartment.value;
    var reg = "^[1-9]+$";
    if (apVal.match(reg)) {
        $("#apSp").text("");
    } else {
        $("#apSp").text("Only numbers");
    }


}
function inputTelAction(tel) {
    var telVal = tel.value;
    var telReg = "[0-9]{9}";
    if (telVal.match(telReg)) {
        $("#telSp").text("");
        $(tel).addClass("ok");
    } else {
        $(tel).removeClass("ok");
        $("#telSp").text("Wrong phone number format");
    }
    if ($(".ok").size() === 8) {
        $('#button').prop("disabled", false);
    } else {
        $('#button').prop("disabled", true);
    }
}
function inputPasswordAction(input) {
    password = input.value;

    var passwReg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;

    if (password.match(passwReg)) {
        $("#status").removeClass("bg-danger");
        $("#status").addClass("bg-success").text("password match");
        $(input).addClass("ok");
    } else {
        $("#status").removeClass("bg-success");
        $("#status").text("At least one numeric digit, one uppercase and one lowercase letter").addClass("bg-danger");
//                          
        $(input).removeClass("ok");
    }
    if (passwordRepeat !== '') {

        if (passwordRepeat === password) {
            $("#statusRepeat").removeClass("bg-danger");
            $("#statusRepeat").text("Ok").addClass("bg-success");
            $("#passwordRepeat").addClass("ok");
        } else {
            $("#statusRepeat").removeClass("bg-success");
            $("#statusRepeat").addClass("bg-danger").text(" Not Ok");
            $("#passwordRepeat").removeClass("ok");
        }
    }
    if ($(".ok").size() === 8) {
        $('#button').prop("disabled", false);
    } else {
        $('#button').prop("disabled", true);
    }

}
function repeatPasswordAction(repeat) {
    passwordRepeat = repeat.value;
    password = $('#password').val();
    if (password === '') {
        $("#status").text("please enter password");
        $("#passwordRepeat").removeClass("ok");
    } else {
        if (passwordRepeat === password) {
            $("#statusRepeat").removeClass("bg-danger");
            $("#statusRepeat").text("Ok").addClass("bg-success");
            $("#passwordRepeat").addClass("ok");
        } else {
            $("#statusRepeat").removeClass("bg-success");
            $("#statusRepeat").text(" Not Ok").addClass("bg-danger");
            $("#passwordRepeat").removeClass("ok");
        }
    }
    if ($(".ok").size() === 8) {
        $('#button').prop("disabled", false);
    } else {
        $('#button').prop("disabled", true);
    }

}
function inputLoginAction(login) {
    var loginReg = /^(?=.*[a-zA-Z].*)([a-zA-Z0-9]{3,10})$/;
    var loginString = login.value;
    if (loginString.match(loginReg)) {
        $("#loginSp").text("");
        $(login).addClass("ok");
    } else {
        $("#loginSp").text("Only latin letters and digits from 3 to 10 symbols");
        $(login).removeClass("ok");
    }
    if ($(".ok").size() === 8) {
        $('#button').prop("disabled", false);
    } else {
        $('#button').prop("disabled", true);
    }

}
function inputNameAction(name) {
    var nameValue = name.value;
    if (nameValue.match(snReg)) {
        $("#nameSp").text("");
        $(name).addClass("ok");
    } else {
        $("#nameSp").text("Change name");
    }
    if ($(".ok").size() === 8) {
        $('#button').prop("disabled", false);
    } else {
        $('#button').prop("disabled", true);
    }
}

function inputSurnameAction(surname) {
    var surnameValue = surname.value;
    if (surnameValue.match(snReg)) {
        $("#surname").text("");
        $(surname).addClass("ok");
    } else {
        $("#surname").text("Change surname");
        $(surname).removeClass("ok");
    }

    if ($(".ok").size() === 8) {
        $('#button').prop("disabled", false);
    } else {
        $('#button').prop("disabled", true);
    }
}