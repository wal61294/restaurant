/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


window.onload = function () {
    

};
function showDoneOrders() {
    var elem = $("#done-orders");
    if ($(elem).is(':visible')) {
        $(elem).slideUp();
    } else {
        $(elem).slideDown();
    }
}
function showCancelled() {
    var elem = $("#cancelled-orders");
    if ($(elem).is(':visible')) {
        $(elem).slideUp();
    } else {
        $(elem).slideDown();
    }
}
function showCancelledByYou() {
    var elem = $("#cancelled-by-you");
    if ($(elem).is(':visible')) {
        $(elem).slideUp();
    } else {
        $(elem).slideDown();
    }
}
function showInTransit() {
    var elem = $("#undone-user-orders");
    if ($(elem).is(':visible')) {
        $(elem).slideUp();
    } else {
        $(elem).slideDown();
    }
}
function showNew() {
    var elem = $("#newOrdersTable");
    if ($(elem).is(':visible')) {
        $(elem).slideUp();
    } else {
        $(elem).slideDown();
    }
}