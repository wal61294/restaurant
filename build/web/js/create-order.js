
var total;

var number;
var priceEl;
window.onload = function () {
    total = $("#total");
    total.text(0);
    $("#buttonCreateOrder").prop("disabled", true);
   
};

var price = 0;
function calculatePrice() {
    var price = 0;
    var liS = document.getElementsByClassName("productLI");
    for (var i = 0; i < liS.length; i++) {
        number = $(liS[i]).children("input").val();
        priceEl = $(liS[i]).children(".price").text();
        price += parseInt(priceEl) * parseInt(number);
    }
    if (price === 0) {
        $("#buttonCreateOrder").prop("disabled", true);
    } else {
        $("#buttonCreateOrder").prop("disabled", false);
    }
    total.text(price);

}
function showUL(span){
    
    var ul=$(span).siblings("ul")[0];
    if($(ul).is(':visible')){
        $(ul).slideUp();
    }else{
        $(ul).slideDown();
    }
    
}


