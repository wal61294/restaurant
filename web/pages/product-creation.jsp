<%-- 
    Document   : product-creation
    Created on : 16.03.2016, 23:20:02
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="addinProduct"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h2><fmt:message key="addinProduct"/></h2>
        <form method="post" id="productCreationForm" action="controller">
            <p><input class="form-control" type="hidden" name="action" value="createProduct"/> 
            <p><input class="form-control" pattern="[-&quot«A-ZА-Яa-zа-я\s»]{1,50}" required  name="productName"  type="text" size="35" placeholder="<fmt:message key="input.product"/>" /><span>${productError}</span> 

            <p><span><fmt:message key="category"/>:</span>   
                <select  class="form-control" required name="category" id="category" form="productCreationForm">
                    <option value="" selected >Choose here</option>
                    <c:forEach var="category" items="${productCategories}">
                        <option value="${category.name}">${category.name}</option>
                    </c:forEach>
                </select>

            <p><input required min="1" class="form-control" name="productMass" type="number" size="35" placeholder="<fmt:message key="input.mass"/>" /> 
            <p><input required min="1" class="form-control" name="productPrice" type="number" size="35" placeholder="<fmt:message key="input.price"/>" /> 
            <p><input class="btn btn-primary updprim btadpr" id="button" type="submit" value="<fmt:message key="add"/>"/>
        </form>
        <form method="post" action="controller">
            <input type="hidden" name="action" value="editProducts">
            <input type="submit" class="btn-link" value="<fmt:message key="back"/>">
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <footer><ct:custom/></footer>
    </body>
</html>
