<%-- 
    Document   : client
    Created on : 03.03.2016, 20:58:20
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages"/>

<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
        <title><fmt:message key="welcome.client"/></title>
    </head>
    <body>
        
        <h3><fmt:message key="welcome.client"/> ${name}!</h3>
        <form  method="POST" action="controller">
            <p align="center">
                <input type="hidden" name="action" value="viewPersonalData"/>
                <input class="clientImageSubmit" type="image" src="resources/image/kzi.png" value="Изменить персональные данные"/>		
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="center">
                <input type="hidden" name="action" value="goToOrderCreation"/>
                <input class="clientImageSubmit" type="image" src="resources/image/KW56oVJRpLk.jpg" class="edit" value="Создать заказ"/>		
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="center">
                <input type="hidden" name="action" value="manageClientOrders"/>
                <input class="clientImageSubmit" type="image" src="resources/image/23174647.png"  value="Редактировать заказы" />		
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input  class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <footer><ct:custom/></footer>
    </body>
</html>
