<%-- 
    Document   : change-location
    Created on : 10.04.2016, 15:19:36
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
        <title><fmt:message key="changeloc"/></title>
    </head>
    <body>
        <h2><fmt:message key="changeloc"/></h2>
        <form id="change-location-form" method="post" action="controller">
            <p><input type="hidden" name="action" value="changeLocation"/> 
            <p><label for="street" ><fmt:message key="street"/>:</label><input class="form-control" style="text-transform: capitalize;" required pattern="^[А-Яа-я _]*[А-Яа-я][А-Яа-я _]*$" type="login" size="35" maxlength="30" onchange="inputStreetAction(this)" id="street" value="${street}" name="street"/><span  class="bg-danger" id="stSp">${streetError}</span>
            <p><label for="house" ><fmt:message key="house"/>:</label><input class="form-control" required pattern="^[0-9][0-9/А-Яа-я]+"  type="login" size="5" maxlength="5" id="house" value="${house}" onchange="inputHouseAction(this)" name="house"/><span id="houseSp" class="bg-danger">${houseError}</span>
            <p><label for="apartment" ><fmt:message key="apartment"/>:</label><input class="form-control"  pattern="^[0-9]+$" type="login" size="5" id="apartment" value="${apartment}" onchange="inputApartmentAction(this)"name="apartment"/><span id="apSp" class="bg-danger">${apartmentError}</span>
            <p><label for="password" ><fmt:message key="password"/>:</label><input  class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}" required id="password"  onchange="inputPasswordAction(this)" class="${ok}" name="password"  type="password" size="35"  value="${password}"/><span  class="bg-danger" id="status"></span>
            <p><input id="button" class="btn btn-primary changeprim" type="submit" value="<fmt:message key="change"/>"/>
        </form>
        <form  method="POST" action="controller">
            <p align="center">
                <input type="hidden" name="action" value="viewPersonalData"/>
                <input class="btn-link" type="submit" class="edit" value="<fmt:message key="back"/>"/>		
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToStartPage"/>
                <input class="goToStartPage btn-link" type="submit" value="<fmt:message key="gotostart"/>" >
            </p>
        </form>
        <footer><ct:custom/></footer>
    </body>
</html>
