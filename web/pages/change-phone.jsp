<%-- 
    Document   : change-phone
    Created on : 10.04.2016, 15:18:10
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="changephone"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h2><fmt:message key="changephone"/></h2>
        <form method="post" action="controller" id="phone-change-form">
            <p><input type="hidden" name="action" value="changePhone"/> 
            <p><label for="tel" ><fmt:message key="phone"/>:</label><span id="telIndex">+375</span><input class="form-control" required pattern="[0-9]{9}" id="tel" minlength="9" size="9" maxlength="9" onchange="inputTelAction(this)" value="${phone}" name="phone"/><span class="bg-danger" id="telSp"></span>
            <p><label for="password" ><fmt:message key="password"/>:</label><input class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}" required id="password"  onchange="inputPasswordAction(this)" class="${ok}" name="password"  type="password" size="35"  value="${password}"/><span class="bg-danger" id="status"></span>
            <p><input id="button" type="submit" value="<fmt:message key="change"/>" class="btn btn-primary changeprim"/>
        </form>
        <form  method="POST" action="controller">
            <p align="center">
                <input type="hidden" name="action" value="viewPersonalData"/>
                <input type="submit" class="btn-link" value="<fmt:message key="back"/>"/>		
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToStartPage"/>
                <input class="goToStartPage btn-link" type="submit" value="<fmt:message key="gotostart"/>" >
            </p>
        </form>
        <footer><ct:custom/></footer>
            
    </body>
</html>
