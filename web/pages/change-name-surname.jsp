<%-- 
    Document   : change-name-surname
    Created on : 10.04.2016, 12:06:06
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="changens"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h2><fmt:message key="changens"/></h2>
        <form id="change-name-surname-form" method="post" action="controller">
            <p><input type="hidden" name="action" value="changeNameSurname"/> 
            <p><label for="name" ><fmt:message key="name"/>:</label><input class="form-control" style="text-transform: capitalize;" id="name" required pattern="[-A-ZА-Яa-zа-я]{1,30}" name="name" onchange="inputNameAction(this)" class="${ok}" type="text" maxlength="35" size="35"  value="${name}"/><span id="nameSp" class="bg-danger">${nameError}</span>
            <p><label for="surnameInp" ><fmt:message key="surname"/>:</label><input class="form-control"style="text-transform: capitalize;" required pattern="[-A-ZА-Яa-zа-я]{1,30}"   onchange="inputSurnameAction(this)" class="${ok}" name="surname" type="text" maxlength="35" size="35" id="surnameInp" value="${surname}"/><span id="surname" class="bg-danger">${surnameError}</span>
            <p><label for="password" ><fmt:message key="password"/>:</label><input class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}" required id="password"  onchange="inputPasswordAction(this)" class="${ok}" name="password"  type="password" size="35"  value="${password}"/><span id="status" class="bg-danger"></span>
            <p><input class="btn btn-primary changeprim" id="button" type="submit" value="<fmt:message key="change"/>"/>
        </form>
        <form  method="POST" action="controller">
            <p align="center">
                <input type="hidden" name="action" value="viewPersonalData"/>
                <input class="btn-link" type="submit" class="edit" value="<fmt:message key="back"/>"/>		
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToStartPage"/>
                <input class="goToStartPage btn-link" type="submit" value="<fmt:message key="gotostart"/>" >
            </p>
        </form>
        <footer><ct:custom/></footer>
    </body>
</html>
