<%-- 
    Document   : view-products
    Created on : 05.03.2016, 18:29:24
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="manprod"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src='js/jquery-2.2.1.min.js'></script>
        <script type="text/javascript" src='js/product_organisation.js'></script>
    </head>
    <body>
        <h2><fmt:message key="manprod"/></h2>
        <ul id="productsUl">
            <c:forEach var="category" items="${categoryProducts}">
                <li >
                    <span onclick="showUL(this)" class="categoryname">${category.name}<span class="hsdesc"> <fmt:message key="shhi"/></span></span>
                    <c:choose>
                        <c:when test="${category.visible=='true'}">
                            <form  method="POST" action="controller" class="productsForm">
                                <input type="hidden" name="action" value="setCategoryInVisible"/>
                                <input type="hidden" name="categoryName" value="${category.name}"/>
                                <input  class=" category-btn docategoryInvisible btn  btn-danger" type="submit" value="<fmt:message key="doinvis"/>" >                                  
                            </form>
                        </c:when>
                        <c:otherwise>
                            <form  method="POST" action="controller" class="productsForm">
                                <input type="hidden" name="action" value="setCategoryVisible"/>
                                <input type="hidden" name="categoryName" value="${category.name}"/>
                                <input  class=" category-btn docategoryVisible btn btn-success" type="submit" value="<fmt:message key="dovis"/>" >                                  
                            </form>

                        </c:otherwise>

                    </c:choose>

                    <ul class="list-group">
                        <li class="list-group-item head-products">
                            <span class="name"><fmt:message key="food.name"/></span>
                            <span class="price"><fmt:message key="price"/></span>
                            <span class="mass"><fmt:message key="mass"/></span>
                        </li>
                        <c:forEach var="product" items="${category.products}">
                            <li class="productLI list-group-item">
                                <span class="name">${product.name}</span>
                                <span class="price">${product.price}</span>
                                <span class="mass">${product.mass}</span>
                                <c:choose>
                                    <c:when test="${product.inMenu=='true'}">
                                        <form  method="POST" action="controller" class="productsForm">
                                            <input type="hidden" name="action" value="deleteFromMenu"/>
                                            <input type="hidden" name="productId" value="${product.id}"/>
                                            <input  class="deletefromMenu btn" type="submit" value="<fmt:message key="deleteFromMenu"/>" >                                  
                                        </form>
                                    </c:when>    
                                    <c:otherwise>
                                        <form  method="POST" action="controller" class="productsForm">                              
                                            <input type="hidden" name="action" value="addToMenu"/>
                                            <input type="hidden" name="productId" value="${product.id}"/>
                                            <input  class="AddToMenu btn"type="submit" value="<fmt:message key="addToMenu"/>" >         
                                        </form>
                                    </c:otherwise>
                                </c:choose>
                                <form  method="POST" action="controller" class="productsForm">                              
                                    <input type="hidden" name="action" value="goToProductUpdate"/>
                                    <input type="hidden" name="productId" value="${product.id}"/>
                                    <input  class="btn btninl btn-warning" type="submit" value="<fmt:message key="change"/>" >         
                                </form>

                            </li>
                        </c:forEach>
                    </ul>
                </li>

            </c:forEach>
        </ul>
        <form  method="POST" action="controller" class="productsForm">
            <p align="right">
                <input type="hidden" name="action" value="goToProductCreation"/>
                <input  type="submit" id="newProduct" value="<fmt:message key="addProduct"/>" class ="btn btn-success " >
            </p>
        </form>
        <form  method="POST" action="controller" class="productsForm">
            <p align="center">
                <input type="hidden" name="action" value="addCategories"/>
                <input type="submit" id="newCategory"  class ="btn btn-primary" value="<fmt:message key="addCategory"/>" />		
            </p>
        </form>

        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input type="submit" class="btn-danger logout" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToStartPage"/>
                <input type="submit" class="btn-link" value="<fmt:message key="back"/>" >
            </p>
        </form>
        <footer id="static-footer" class="product-footer"><ct:custom/></footer>
    </body>
</html>
