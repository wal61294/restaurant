<%-- 
    Document   : change-password
    Created on : 10.04.2016, 15:17:25
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="changeps"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h2><title><fmt:message key="changeps"/></title></h2>
        <form method="post" action="controller" id="changePasswordForm">
            <p><input type="hidden" name="action" value="changePassword"/> 
            <p><label for="password" ><fmt:message key="password"/>:</label><input class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}" required id="password"  onchange="inputPasswordAction(this)" class="${ok}" name="password"  type="password" size="35"  value="${password}"/><span id="status" class="bg-danger">${oldPasswordError}</span>
            <p><label for="newPassword" ><fmt:message key="newpassw"/>:</label><input class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}" required id="newPassword"  onchange="inputPasswordAction(this)" class="${ok}" name="newPassword"  type="password" size="35"  value="${password}"/><span id="status" class="bg-danger"></span>
            <p><label for="repeatNewPassword" ><fmt:message key="password.repeat"/>:</label><input class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}"  required id="repeatNewPassword"  onchange="repeatPasswordAction(this)" name="repeatNewPassword" type="password" size="35" /> <span id="statusRepeat" class="bg-danger">${passwordRepeatError}</span>
            <p><input id="button" class="btn btn-primary changeprim" type="submit" value="<fmt:message key="change"/>"/>
        </form>
        <form  method="POST" action="controller">
            <p align="center">
                <input type="hidden" name="action" value="viewPersonalData"/>
                <input type="submit" class="btn-link" value="<fmt:message key="back"/>"/>		
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToStartPage"/>
                <input class="goToStartPage btn-link" type="submit" value="<fmt:message key="gotostart"/>" >
            </p>
        </form>
        <footer><ct:custom/></footer>
    </body>
</html>
