<%-- 
    Document   : error
    Created on : 01.03.2016, 20:20:46
    Author     : al94
--%>

<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Error</title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body id="bodyError">
        <h1 id="h1Err">Error</h1>
        <p>${property}<br>
            ${error}
        </p>
    </body>
</html>
