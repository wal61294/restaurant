<%-- 
    Document   : view-client-data
    Created on : 29.06.2016, 10:15:07
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="vcd"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h2><fmt:message key="pdata"/> ${login}:</h2>
        <h4 id="firsth4"><fmt:message key="name"/>: ${name}</h4>
        <h4><fmt:message key="surname"/>: ${surname}</h4>
        <h4><fmt:message key="phone"/>: +375${phone}</h4>
        <h4><fmt:message key="adress"/>: ${street}  ${house} ${apartment}.</h4>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input  class="btn-danger logout" type="submit" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="editOrders"/>
                <input type="submit" class="btn-link" value="<fmt:message key="back"/>" >
            </p>
        </form>
        <footer><ct:custom/></footer>
    </body>
</html>
