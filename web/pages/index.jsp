<%-- 
    Document   : index
    Created on : 24.01.2016, 12:40:03
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html lang="${language}">
    <head>
        <title><fmt:message key="h1.entry"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>

    <body id="bodyStart">
        <form class="changelang">
            <select id="language" name="language" onchange="submit()">
                <option value="ru" ${language == 'ru' ? 'selected' : ''}>RU</option>
                <option value="en" ${language == 'en' ? 'selected' : ''}>EN</option>
            </select>
        </form>
        <h1 id="h1FromStart"><fmt:message key="h1.entry"/></h1>
        <div id="logo"></div>
        <div id="formDivStart">
            <form  action="controller" method="post">
                <input type="hidden" name="action" value="login"/>
                <p> <input required name="login" type="login" size="35" class="form-control" placeholder="<fmt:message key="input.login"/>" title="<fmt:message key="input.login"/>"/></p>
                <p><input required name="password" type="password" size="35" class="form-control" title="<fmt:message key="input.password"/>" placeholder="<fmt:message key="input.password"/>"/></p>
                <p><input  type="submit"                               
                           value="<fmt:message key="button.login"/>" class="btn btn-primary"/><p/> 
            </form>   
            <span  id="login-alert" class="bg-danger">${errorLoginPassMessage}</span>
            <form id="gotoRegisterForm" action="controller" method="post">
                <input type="hidden" name="action" value="gotoregister" />
                <input  type="submit"                               
                        value="<fmt:message key="button.gotoregister"/>" class="btn btn-default regbut"/>
            </form>
        </div>
        <footer><ct:custom/></footer>
    </body>
</html>
