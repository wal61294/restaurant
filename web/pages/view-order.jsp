<%-- 
    Document   : view-order
    Created on : 26.03.2016, 15:34:17
    Author     : al94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tag/customTag.tld" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties/messages" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="vieworder"/></title>
        <link href="resources/bootstrap/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resources/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h2><fmt:message key="vieworder"/></h2>
        <div class="succcessType">  ${userType}</div>   
        <ul class="list-group orderlist">
            <li class="productLI list-group-item olhead">
                <div class="name"><fmt:message key="food.name"/></div>
                <div class="mass"><fmt:message key="mass"/></div>
                <div class="number"><fmt:message key="amount"/></div>     
            </li>
            <c:forEach var="productWithItsNumber" items="${productsWithTheirsNumbers}">

                <li class="productLI list-group-item">
                    <div class="name">${productWithItsNumber.key.name}</div>
                    <div class="mass">${productWithItsNumber.key.mass}</div>
                    <div class="number">${productWithItsNumber.value}</div>     
                </li>

            </c:forEach>
        </ul>
        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="logout"/>
                <input type="submit" class="btn-danger logout" value="<fmt:message key="logout"/>" >
            </p>
        </form>
        <span id="totalSpan"><fmt:message key="total"/>:  ${totalPrice}<span id="total"></span><fmt:message key="rubles"/> </span>


        <form  method="POST" action="controller">
            <p align="right">
                <input type="hidden" name="action" value="goToStartPage"/>
                <input type="submit" class="goToStartPage btn-link" value="<fmt:message key="gotostart"/>" >
            </p>
        </form>


        <c:choose>
            <c:when test="${userType=='client'}">
                <form  method="POST" action="controller">
                    <p align="center">
                        <input type="hidden" name="action" value="manageClientOrders"/>
                        <input type="submit" class="btn-link" value="<fmt:message key="back"/>" />		
                    </p>
                </form>
            </c:when>
            <c:when test="${userType=='admin'}">
                <form  method="POST" action="controller">
                    <p align="center">
                        <input type="hidden" name="action" value="editOrders"/>
                        <input type="submit" class="btn-link" value="<fmt:message key="back"/>" />		
                    </p>
                </form>
            </c:when>
        </c:choose>
        <footer id="static-footer"><ct:custom/></footer>
    </body>
</html>
