/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.huzarevich.restaurant.test;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IProductDAO;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * JUnit test for ProductDAO
 *
 * @author al94
 */
public class TestProductDAO {

    private static IProductDAO productDao;
    
    /**
     * initializes ProductDao 
     */
    @BeforeClass
    public static void init() {
        productDao = DAOFactory.getDAOFactory(DAOEnum.MYSQL).getProductDao();
    }
    /**
     * @Test for ProductDao findById
     *
     *
     * @throws com.epam.huzarevich.restaurant.exceptions.DAOException
     * @throws com.epam.huzarevich.restaurant.exceptions.PoolException
     */

    @Test
    public void testFindById() throws DAOException, PoolException {
        assertEquals("Coca-Cola", productDao.getById(1).getName());
    }
}
