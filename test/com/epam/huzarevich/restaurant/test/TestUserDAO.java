package com.epam.huzarevich.restaurant.test;

import com.epam.huzarevich.restaurant.dao.factory.DAOEnum;
import com.epam.huzarevich.restaurant.dao.factory.DAOFactory;
import com.epam.huzarevich.restaurant.dao.interfaces.IUserDAO;
import com.epam.huzarevich.restaurant.exceptions.DAOException;
import com.epam.huzarevich.restaurant.exceptions.PoolException;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * JUnit test for UserDAO
 *
 * @author al94
 */
public class TestUserDAO {

    private static IUserDAO userDao;

    /**
     * initializes UserDao 
     */
    @BeforeClass
    public static void init() {
        userDao = DAOFactory.getDAOFactory(DAOEnum.MYSQL).getUserDao();
    }
    /**
     * @Test for UserDao findByLogin
     *
     *
     * @throws com.epam.huzarevich.restaurant.exceptions.DAOException
     * @throws com.epam.huzarevich.restaurant.exceptions.PoolException
     */
    @Test
    public void testFind() throws DAOException, PoolException {
        assertEquals("al21", userDao.findByLogin("al21").getLogin());
    }
    /**
     * @Test for UserDao getById
     *
     *
     * @throws com.epam.huzarevich.restaurant.exceptions.DAOException
     * @throws com.epam.huzarevich.restaurant.exceptions.PoolException
     */
    @Test
    public void testGetById() throws DAOException, PoolException {
        assertEquals("al21", userDao.getById(1).getLogin());
    }
}
